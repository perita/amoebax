# The minimum CMake version that this project requires.
cmake_minimum_required(VERSION 2.8)

# The project name used as the name for some variables as well
# as the name for Visual Studios' solution file, among others.
project(balls)

# This is the information stored in Info.plist for Mac OS X.
set(MACOSX_BUNDLE_INFO_STRING "Example game for Benzaiten")
#set(MACOSX_BUNDLE_ICON_FILE "balls.icns")
set(MACOSX_BUNDLE_GUI_IDENTIFIER "com.geishastudios.benzaiten.examples.balls")
set(MACOSX_BUNDLE_LONG_VERSION_STRING "1.0")
set(MACOSX_BUNDLE_BUNDLE_NAME "Balls")
set(MACOSX_BUNDLE_SHORT_VERSION_STRING "1.0")
set(MACOSX_BUNDLE_BUNDLE_VERSION "1")
set(MACOSX_BUNDLE_COPYRIGHT "Copyright (c) 2009, 2010, 2012 Geisha Studios")

# Tell CMake where to find additional macros and module files.
list(APPEND CMAKE_MODULE_PATH ${Benzaiten_SOURCE_DIR}/cmake)

# Process subdirectories.
add_subdirectory(assets)
add_subdirectory(src)
