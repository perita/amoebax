//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "SpriteGroup.hpp"
#include <boost/foreach.hpp>
#include "Context.hpp"

using namespace benzaiten;

SpriteGroup::SpriteGroup (double x, double y)
    : Sprite (x, y)
    , members ()
{
}

Sprite::Ptr SpriteGroup::add (const Sprite::Ptr &sprite) {
    this->members.push_back (Member (sprite));
    sprite->moveTo (this->x () + sprite->x (), this->y () + sprite->y ());
    if (this->context) {
        this->context->add (sprite);
    }
    return sprite;
}

void SpriteGroup::added () {
    BOOST_FOREACH (const Member &member, this->members) {
        this->context->add (member.sprite);
    }
}

void SpriteGroup::moveToX (double x) {
    Sprite::moveToX (x);
    BOOST_FOREACH (const Member &member, this->members) {
        member.sprite->moveToX (x + member.x);
    }
}

void SpriteGroup::moveToY (double y) {
    Sprite::moveToY (y);
    BOOST_FOREACH (const Member &member, this->members) {
        member.sprite->moveToY (y + member.y);
    }
}

void SpriteGroup::removed () {
    BOOST_FOREACH (const Member &member, this->members) {
        this->context->remove (member.sprite.get ());
    }
}

void SpriteGroup::setVisible (bool visible) {
    Sprite::setVisible (visible);
    BOOST_FOREACH (const Member &member, this->members) {
        member.sprite->setVisible (visible);
    }
}
