//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_SPRITE_TYPE_ITERATOR_HPP)
#define GEISHA_STUDIOS_BENZAITEN_SPRITE_TYPE_ITERATOR_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <boost/iterator/iterator_facade.hpp>
#include <utility>
#include "Context.hpp"
#include "Sprite.hpp"

namespace benzaiten {
    ///
    /// @class SpriteTypeIterator
    /// @brief Iterator for linked Sprites of a given type.
    ///
    template <class SpriteClass> class SpriteTypeIterator:
            public boost::iterator_facade<
                SpriteTypeIterator<SpriteClass>,
                SpriteClass,
                boost::bidirectional_traversal_tag> {
        public:
            SpriteTypeIterator ()
                : sprite (0)
            { }

            SpriteTypeIterator (SpriteClass *sprite)
                : sprite (sprite)
            { }

        private:
            friend class boost::iterator_core_access;

            void decrement () {
                this->sprite =
                    reinterpret_cast<SpriteClass *>(this->sprite->type_prev);
            }

            SpriteClass &dereference() const {
                return *sprite;
            }

            bool equal (const SpriteTypeIterator &other) const {
                return this->sprite == other.sprite;
            }

            void increment() {
                this->sprite =
                    reinterpret_cast<SpriteClass *>(this->sprite->type_next);
            }

            SpriteClass *sprite;
    };

    ///
    /// @brief Gets the Sprites of an specified type from a Context.
    ///
    /// @param[in] context The Context to retrieve the Sprites from.
    /// @param[in] type The type of the Sprites to get.
    ///
    /// @return The pair of first and last sprites that are of type
    /// @p type.
    ///
    template <class SpriteClass> std::pair<SpriteTypeIterator<SpriteClass>, SpriteTypeIterator<SpriteClass> > spritesOfType (
            SpriteClass *sprite) {
        return std::make_pair(
                SpriteTypeIterator<SpriteClass> (sprite),
                SpriteTypeIterator<SpriteClass> ());
    }
}

#endif // !GEISHA_STUDIOS_BENZAITEN_SPRITE_TYPE_ITERATOR_HPP
