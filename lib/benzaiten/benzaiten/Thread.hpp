//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_THREAD_HPP)
#define GEISHA_STUDIOS_BENZAITEN_THREAD_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <cassert>
#include <SDL_thread.h>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>

namespace benzaiten {
    namespace SDL {
        /// The pointer to a condition variable.
        typedef boost::shared_ptr<SDL_cond> Cond;
        /// The pointer to a mutex.
        typedef boost::shared_ptr<SDL_mutex> Mutex;

        ///
        /// @class ScopedLock
        /// @brief A RAII-like lock class for a mutex.
        ///
        class ScopedLock: public boost::noncopyable {
            public:
                ///
                /// @brief Gets and locks the mutex.
                ///
                /// @param[inout] mutex The mutex to lock.
                ///
                ScopedLock (const Mutex &mutex)
                    : mutex (mutex) {
                    assert (mutex && "Can't lock a NULL mutex");
                    SDL_LockMutex (this->mutex.get ());
                }

                ///
                /// @brief Releases the lock on the mutex.
                ///
                ~ScopedLock () {
                    SDL_UnlockMutex (this->mutex.get ());
                }

            private:
                /// The mutex that is locked.
                Mutex mutex;
        };

        ///
        /// @brief Creates a new condition variable.
        ///
        /// @return A condition variable in a smart pointer.
        ///
        inline Cond cond () {
            return Cond (SDL_CreateCond (), SDL_DestroyCond);
        }

        ///
        /// @brief Creates a new mutex.
        ///
        /// @return A mutex in a smart pointer.
        ///
        inline Mutex mutex () {
            return Mutex (SDL_CreateMutex (), SDL_DestroyMutex);
        }

        ///
        /// @brief Signals the condition variable.
        ///
        /// @param[in] var The condition variable to singal.
        ///
        inline void signal (const Cond &var) {
            assert (var && "Can't signal a NULL condition variable");
            SDL_CondSignal (var.get ());
        }

        ///
        /// @brief Waits for a condition variable.
        ///
        /// @param[in] var The condition variable to wait for.
        /// @param[in] lock The mutex that is locked to wait for this.
        ///
        inline void wait (const Cond &var, const Mutex &lock) {
            assert (var && "Can't wait a NULL condition variable");
            assert (var && "Can't wait with a NULL mutex");
            SDL_CondWait (var.get (), lock.get ());
        }
    }
}

#endif // !GEISHA_STUDIOS_THREAD_HPP
