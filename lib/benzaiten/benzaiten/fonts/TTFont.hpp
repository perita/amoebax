//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_FONTS_TT_FONT_HPP)
#define GEISHA_STUDIOS_BENZAITEN_FONTS_TT_FONT_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include <SDL_ttf.h>
#include <boost/shared_ptr.hpp>
#include "../Font.hpp"

namespace benzaiten {

    namespace SDL {
        /// The pointer to a font.
        typedef boost::shared_ptr<TTF_Font> Font;

        ///
        /// @brief Creates a new Font.
        ///
        /// @param[in] font_file The path to the TTF file to open.
        /// @param[in] size The font size.
        ///
        /// @return A font in a smart pointer.
        ///
        inline Font font (const std::string &font_file, int size) {
            if (!TTF_WasInit ()) {
                TTF_Init ();
            }
            return Font (
                    TTF_OpenFont (font_file.c_str (), size),
                    TTF_CloseFont);
        }
    }

    ///
    /// @class TTFont
    /// @brief A TrueType font.
    ///
    class TTFont: public Font {
        public:
            typedef boost::shared_ptr<TTFont> Ptr;

            ///
            /// @brief Creates a new TrueType font.
            ///
            /// @param font_name The name of the font, excluding the path and
            ///        the file extension, which must be `.ttf'
            /// @param pt_size The size of the font.
            ///
            /// @return The smart pointer with the newly created font.
            ///
            static Ptr New (const std::string &font_name, int pt_size) {
                return Ptr (new TTFont (font_name, pt_size));
            }

            virtual int height () const;
            virtual SDL::Texture renderText (const std::string &text) const;

            ///
            /// @brief Sets the color for the rendered text.
            ///
            /// @param[in] color The color for the text.
            ///
            void setTextColor (const SDL_Color &color);

            virtual int width (const std::string &text) const;

        protected:
            ///
            /// @brief Constructor.
            ///
            /// @param font_name The name of the font, excluding the path and
            ///        the file extension, which must be `.ttf'
            /// @param pt_size The size of the font.
            ///
            TTFont (const std::string &font_name, int pt_size);

        private:
            /// The actual font to use.
            SDL::Font font;
            /// The color to use to render the text.
            SDL_Color text_color;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_FONTS_TT_FONT_HPP
