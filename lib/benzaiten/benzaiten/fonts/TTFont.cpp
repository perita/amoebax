//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "TTFont.hpp"
#include <stdexcept>
#include "../Game.hpp"
#include "../Utils.hpp"

using namespace benzaiten;

TTFont::TTFont (const std::string &font_name, int pt_size)
    : Font ()
    , font (SDL::font (
                Game::resources ().getFilePath ("fonts", font_name + ".ttf"),
                pt_size))
    , text_color (makeColor (0, 0, 0))
{
    if (!font) {
        throw std::runtime_error ("Couldn't open font " + font_name);
    }
}

int
TTFont::height () const
{
    return TTF_FontHeight (this->font.get ());
}

SDL::Texture
TTFont::renderText (const std::string &text) const
{
    SDL::Surface surface (SDL::surface (
                TTF_RenderUTF8_Blended (this->font.get (), text.c_str (),
                    this->text_color)));
    return SDL::texture (Game::screen ().renderer.get (), surface);
}

void
TTFont::setTextColor (const SDL_Color &color) {
    this->text_color = color;
}

int
TTFont::width (const std::string &text) const
{
    int height = 0;
    int width = 0;
    TTF_SizeUTF8 (this->font.get (), text.c_str (), &width, &height);
    return width;
}
