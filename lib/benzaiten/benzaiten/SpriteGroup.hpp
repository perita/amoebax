//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_SPRITE_GROUP_HPP)
#define GEISHA_STUDIOS_BENZAITEN_SPRITE_GROUP_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <vector>
#include <boost/shared_ptr.hpp>
#include "Sprite.hpp"

namespace benzaiten {
    ///
    /// @class SpriteGroup
    /// @brief A group of Sprite objects that act like a single Sprite.
    ///
    class SpriteGroup: public Sprite {
        public:
            /// A smart pointer to a SpriteGroup.
            typedef boost::shared_ptr<SpriteGroup> Ptr;

            ///
            /// @brief Creates a new SpriteGroup with a position.
            ///
            /// @param[in] x The initial X position of the SpriteGroup.
            /// @param[in] y The initial Y position of the SpriteGroup.
            ///
            static Ptr New (double x = 0.0, double y = 0.0) {
                return Ptr (new SpriteGroup (x, y));
            }

            ///
            /// @brief Adds a Sprite to the group.
            ///
            /// The current position of the Sprite is taken as the relative
            /// position that the sprite should have within the group.
            //
            /// @param[in] sprite The Sprite to add.
            ///
            /// @return The Sprite added.
            Sprite::Ptr add (const Sprite::Ptr &sprite);

            virtual void added ();
            virtual void moveToX (double x);
            virtual void moveToY (double y);
            virtual void removed ();
            virtual void setVisible (bool visible);

        protected:
            ///
            /// @brief Creates a new SpriteGroup with a position.
            ///
            /// @param[in] x The initial X position of the SpriteGroup.
            /// @param[in] y The initial Y position of the SpriteGroup.
            ///
            explicit SpriteGroup (double x = 0.0, double y = 0.0);

        private:
            ///
            /// @struct Member
            /// @brief A member of the group.
            ///
            struct Member {
                /// The sprite that is member of this group.
                Sprite::Ptr sprite;

                /// The relative X position of this member respect the group.
                double x;

                /// The relative Y position of this member respect the group.
                double y;

                ///
                /// @brief Constructor.
                ///
                /// The current position of the Sprite is taken as the relative
                /// position that it should have within the group.
                //
                /// @param[in] sprite The member's sprite.
                ///
                Member (const Sprite::Ptr &sprite)
                    : sprite (sprite)
                    , x (sprite->x ())
                    , y (sprite->y ())
                {
                }
            };

            /// The sprite members of this group.
            std::vector<Member> members;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_SPRITE_GROUP_HPP
