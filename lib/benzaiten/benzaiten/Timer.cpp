//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "Timer.hpp"

using namespace benzaiten;

Timer::Timer (double period, Callback callback)
    : callback (callback)
    , current_time (period)
    , period (period)
{
}

bool Timer::isFinished () const {
    return this->current_time <= 0.0;
}

void Timer::reset () {
    this->setPeriod (this->period);
}

void Timer::setCallback (Callback callback) {
    this->callback = callback;
}

void Timer::setPeriod (double period) {
    this->period = period;
    this->current_time = period;
}

bool Timer::update (double elapsed) {
    this->current_time -= elapsed;

    if (this->isFinished ()) {
        this->current_time += this->period;
        if (this->callback && this->callback (*this)) {
            return true;
        } else {
            return false;
        }
    }

    // Until the Timer is done, we must keep it.
    return true;
}
