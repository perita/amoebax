//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "VirtualJoystick.hpp"
#include <cassert>
#include <boost/foreach.hpp>
#include "InputManager.hpp"

using namespace benzaiten;

VirtualJoystick::VirtualJoystick (const TextureAtlas::Ptr &atlas)
    : atlas (atlas)
    , buttons ()
{
    assert (atlas && "The virtual joystick's texture atlas is not correct");
}

void VirtualJoystick::addButton (int action, int x, int y,
        unsigned int frame) {
    this->atlas->setFrame (frame);
    SDL_Rect rect;
    rect.w = this->atlas->width ();
    rect.h = this->atlas->height ();
    rect.x = x - rect.w / 2;
    rect.y = y - rect.h / 2;

    this->buttons.push_back (Button (action, frame, rect));
}

void VirtualJoystick::cleanUp (InputManager &input) {
    BOOST_FOREACH (Button &button, this->buttons) {
        if (button.pressed) {
            input.release (button.action);
            button.pressed = false;
        }
    }
}

bool VirtualJoystick::handleDown (InputManager &input, int pointer_id) {
    BOOST_FOREACH (Button &button, this->buttons) {
        if (input.mouse_x >= button.rect.x &&
                input.mouse_x <= button.rect.x + button.rect.w &&
                input.mouse_y >= button.rect.y &&
                input.mouse_y <= button.rect.y + button.rect.h) {
            if (!button.pressed) {
                input.press (button.action);
                button.pressed = true;
                button.pointer_id = pointer_id;
            }
            return true;
        }
    }
    return false;
}

bool VirtualJoystick::handleUp (InputManager &input, int pointer_id) {
    BOOST_FOREACH (Button &button, this->buttons) {
        if (button.pressed && button.pointer_id == pointer_id) {
            input.release (button.action);
            button.pressed = false;
            return true;
        }
    }
    return false;
}

void VirtualJoystick::render (SDL_Renderer &renderer, const SDL_Rect *clip) const {
    BOOST_FOREACH (const Button &button, this->buttons) {
        this->atlas->setFrame (button.frame);
        this->atlas->render (renderer, clip, button.rect.x, button.rect.y);
    }
}
