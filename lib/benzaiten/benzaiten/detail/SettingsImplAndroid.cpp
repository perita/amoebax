//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "SettingsImplAndroid.hpp"
#include <jni.h>
#include <SDL_stdinc.h>
#include "../SDL.hpp"
#include "../../lib/SDL2/src/core/android/SDL_android.h"

namespace {
    typedef benzaiten::SettingsImplAndroid::Context Context;

    JNIEnv &getEnv () {
        return *(static_cast<JNIEnv *> (Android_JNI_GetEnv ()));
    }

    jclass getActivityClass () {
        return static_cast<jclass> (Android_JNI_GetActivityClass ());
    }

    void Android_JNI_CheckIfExceptionOccurred(JNIEnv &env) {
        jthrowable exception = env.ExceptionOccurred();
        if (exception != NULL) {
            jmethodID mid;

            // Until this happens most JNI operations have undefined behaviour
            env.ExceptionClear();

            jclass exceptionClass = env.GetObjectClass(exception);
            jclass classClass = env.FindClass("java/lang/Class");

            mid = env.GetMethodID(classClass, "getName", "()Ljava/lang/String;");
            jstring exceptionName = (jstring)env.CallObjectMethod(exceptionClass, mid);
            const char* exceptionNameUTF8 = env.GetStringUTFChars(exceptionName, 0);

            mid = env.GetMethodID(exceptionClass, "getMessage", "()Ljava/lang/String;");
            jstring exceptionMessage = (jstring)env.CallObjectMethod(exception, mid);

            if (exceptionMessage != NULL) {
                const char* exceptionMessageUTF8 = env.GetStringUTFChars(
                        exceptionMessage, 0);
                SDL_SetError("%s: %s", exceptionNameUTF8, exceptionMessageUTF8);
                env.ReleaseStringUTFChars(exceptionMessage, exceptionMessageUTF8);
                env.DeleteLocalRef(exceptionMessage);
            } else {
                SDL_SetError("%s", exceptionNameUTF8);
            }

            env.ReleaseStringUTFChars(exceptionName, exceptionNameUTF8);
            env.DeleteLocalRef(exceptionName);
            env.DeleteLocalRef(classClass);
            env.DeleteLocalRef(exceptionClass);
            env.DeleteLocalRef(exception);

            throw benzaiten::SDL::Error ();
        }
    }

    void Android_JNI_DeleteGlobalRef (JNIEnv &env, void *ref) {
        if (ref != 0) {
            env.DeleteGlobalRef (static_cast<jobject> (ref));
        }
    }

    void Android_JNI_DeleteContext (JNIEnv &env, Context &ctx) {
        Android_JNI_DeleteGlobalRef (env, ctx.name_ref);
        Android_JNI_DeleteGlobalRef (env, ctx.shared_preferences);
        Android_JNI_DeleteGlobalRef (env, ctx.preferences_editor);
    }

    void Android_JNI_PreferencesOpen (Context &ctx, const std::string &name) {
        JNIEnv &env = getEnv ();
        env.PushLocalFrame (16);
        try {
            jstring prefName = env.NewStringUTF (name.c_str ());
            ctx.name = prefName;
            ctx.name_ref = env.NewGlobalRef (prefName);

            jclass activity_class = getActivityClass ();
            jmethodID mid = env.GetStaticMethodID (activity_class,
                    "getContext","()Landroid/content/Context;");
            jobject context = env.CallStaticObjectMethod (activity_class, mid);
            Android_JNI_CheckIfExceptionOccurred (env);

            mid = env.GetMethodID (env.GetObjectClass (context),
                    "getSharedPreferences",
                    "(Ljava/lang/String;I)Landroid/content/SharedPreferences;");
            jobject preferences = env.CallObjectMethod (context, mid, prefName, 0);
            Android_JNI_CheckIfExceptionOccurred (env);
            ctx.shared_preferences = env.NewGlobalRef (preferences);

            mid = env.GetMethodID (env.GetObjectClass (preferences),
                    "edit", "()Landroid/content/SharedPreferences$Editor;");
            jobject editor = env.CallObjectMethod (preferences, mid);
            Android_JNI_CheckIfExceptionOccurred (env);
            ctx.preferences_editor = env.NewGlobalRef (editor);

            env.PopLocalFrame (0);
        } catch (benzaiten::SDL::Error &e) {
            Android_JNI_DeleteContext (env, ctx);
            env.PopLocalFrame (0);
            throw;
        }
    }

    void Android_JNI_PreferencesClose (Context &ctx) {
        JNIEnv &env = getEnv ();

        jobject editor = static_cast<jobject> (ctx.preferences_editor);
        jmethodID mid = env.GetMethodID (env.GetObjectClass (editor),
                "commit","()Z");
        env.CallBooleanMethod (editor, mid);

        Android_JNI_DeleteContext (env, ctx);
    }

    int Android_JNI_PreferencesGetInteger (const Context &ctx,
            const std::string &name, int default_value) {

        JNIEnv &env = getEnv ();
        try {
            env.PushLocalFrame (16);
            jstring name_str = env.NewStringUTF (name.c_str ());
            jobject prefs = static_cast<jobject> (ctx.shared_preferences);
            jmethodID mid = env.GetMethodID (env.GetObjectClass (prefs),
                    "getInt", "(Ljava/lang/String;I)I");
            int value = env.CallIntMethod (prefs, mid, name_str, default_value);
            Android_JNI_CheckIfExceptionOccurred (env);
            env.PopLocalFrame (NULL);
            return value;
        } catch (...) {
            env.PopLocalFrame (NULL);
            throw;
        }
        return default_value;
    }

    std::string Android_JNI_PreferencesGetString (const Context &ctx,
            const std::string &name, const std::string &default_value) {

        JNIEnv &env = getEnv ();
        try {
            env.PushLocalFrame (16);
            jstring name_str = env.NewStringUTF (name.c_str ());
            jstring default_str = env.NewStringUTF (default_value.c_str ());
            jobject prefs = static_cast<jobject> (ctx.shared_preferences);
            jmethodID mid = env.GetMethodID (env.GetObjectClass (prefs),
                    "getString",
                    "(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;");
            jstring jvalue = static_cast<jstring> (
                    env.CallObjectMethod (prefs, mid, name_str, default_str));
            const char *cvalue = env.GetStringUTFChars (jvalue, 0);
            std::string value (cvalue);
            env.ReleaseStringUTFChars (jvalue, cvalue);
            env.PopLocalFrame (NULL);
            return value;
        } catch (...) {
            env.PopLocalFrame (NULL);
            throw;
        }
        return default_value;
    }

    void Android_JNI_PreferencesSetInteger (const Context &ctx,
            const std::string &name, int value) {

        JNIEnv &env = getEnv ();
        env.PushLocalFrame (16);
        jstring name_str = env.NewStringUTF (name.c_str ());
        jobject editor = static_cast<jobject> (ctx.preferences_editor);
        jmethodID mid = env.GetMethodID (env.GetObjectClass (editor),
                "putInt",
                "(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;");
        env.CallObjectMethod (editor, mid, name_str, value);
        env.PopLocalFrame (NULL);
    }

    void Android_JNI_PreferencesSetString (const Context &ctx,
            const std::string &name, const std::string &value) {

        JNIEnv &env = getEnv ();
        env.PushLocalFrame (16);
        jstring name_str = env.NewStringUTF (name.c_str ());
        jstring value_str = env.NewStringUTF (value.c_str ());
        jobject editor = static_cast<jobject> (ctx.preferences_editor);
        jmethodID mid = env.GetMethodID (env.GetObjectClass (editor),
                "putString",
                "(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;");
        env.CallObjectMethod (editor, mid, name_str, value_str);
        env.PopLocalFrame (NULL);
    }
}

using namespace benzaiten;

SettingsImplAndroid::SettingsImplAndroid (const std::string &appName)
    : SettingsBase::Impl ()
    , context ()
{
    Android_JNI_PreferencesOpen (this->context, appName);
}

SettingsImplAndroid::~SettingsImplAndroid () {
    Android_JNI_PreferencesClose (this->context);
}


int SettingsImplAndroid::getInteger (const std::string &section,
        const std::string &name, int defaultValue) const {
    try {
        return Android_JNI_PreferencesGetInteger (this->context,
                section + "." + name, defaultValue);
    } catch (...) {
        return defaultValue;
    }
}

std::string SettingsImplAndroid::getString(const std::string &section,
        const std::string &name, const std::string &defaultValue) const {
    try {
        return Android_JNI_PreferencesGetString (this->context,
                section + "." + name, defaultValue);
    } catch (...) {
        return defaultValue;
    }
}

void SettingsImplAndroid::setInteger (const std::string &section,
        const std::string &name, int value) {
    Android_JNI_PreferencesSetInteger (this->context,
            section + "." + name, value);
}

void SettingsImplAndroid::setString(const std::string &section,
        const std::string &name, const std::string &value) {
    Android_JNI_PreferencesSetString (this->context,
            section + "." + name, value);
}
