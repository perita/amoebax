//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "GraphicList.hpp"
#include <boost/foreach.hpp>

using namespace benzaiten;

GraphicList::GraphicList ()
    : Graphic ()
    , graphics ()
{
}

void GraphicList::add (const Graphic::Ptr &graphic, int offset_x,
        int offset_y) {
    this->graphics.push_back (GraphicData (graphic, offset_x, offset_y));
    if (graphic->isActive ()) {
        this->setActive (true);
    }
}

void GraphicList::render (SDL_Renderer &renderer, const SDL_Rect *clip,
        int x, int y) const {
    BOOST_FOREACH (const GraphicData &item, this->graphics) {
        item.graphic->render (renderer, clip, x + item.offset_x, y + item.offset_y);
    }
}

void GraphicList::setAlpha (unsigned char alpha) {
    BOOST_FOREACH (GraphicData &item, this->graphics) {
        item.graphic->setAlpha (alpha);
    }
}

void GraphicList::update (double elapsed) {
    BOOST_FOREACH (GraphicData &item, this->graphics) {
        if (item.graphic->isActive ()) {
            item.graphic->update (elapsed);
        }
    }
}
