//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_SOLID_RECT_HPP)
#define GEISHA_STUDIOS_BENZAITEN_SOLID_RECT_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <boost/shared_ptr.hpp>
#include "../Graphic.hpp"

namespace benzaiten {
    ///
    /// @class SolidRect
    /// @brief Draws a simple rect filled with a single color.
    ///
    class SolidRect: public Graphic {
        public:
            /// A pointer to the SolidRect.
            typedef boost::shared_ptr<SolidRect> Ptr;

            ///
            /// @brief Creates a solid rectangle with the specified color.
            ///
            /// @param[in] rect The dimensions and position of the rectangle.
            /// @param[in] color The color to use to fill the rectangle.
            ///
            /// @return The pointer to the created SolidRect.
            ///
            static Ptr New (const SDL_Rect &rect, const SDL_Color &color) {
                return Ptr (new SolidRect (rect, color));
            }

            virtual void render (SDL_Renderer &render, const SDL_Rect *clip,
                    int x, int y) const;
            virtual void setAlpha (unsigned char alpha);

        protected:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] rect The dimensions and position of the rectangle.
            /// @param[in] color The color to use to fill the rectangle.
            ///
            SolidRect (const SDL_Rect &rect, const SDL_Color &color);

        private:
            /// The alpha to use with the color.
            unsigned char alpha;
            /// The color to fill with.
            SDL_Color color;
            /// The position and dimension of the rectangle.
            SDL_Rect rect;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_SOLID_RECT_HPP
