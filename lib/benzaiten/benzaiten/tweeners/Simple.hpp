//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_TWEENERS_SIMPLE_HPP)
#define GEISHA_STUDIOS_BENZAITEN_TWEENERS_SIMPLE_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>
#include "../Tweener.hpp"

namespace benzaiten { namespace tweener {
    ///
    /// @class Simple
    /// @brief A Tweener for a simple value.
    ///
    class Simple: public Tweener {
        public:
            /// A smart pointer to a Simple tweener.
            typedef boost::shared_ptr<Simple> Ptr;

            /// The setter for the value to tween.
            typedef boost::function<void (double)> Setter;

            /// The function to use to compute the easing.
            typedef boost::function<double (double)> EasingFunction;

            ///
            /// @brief Creates a new Simple tweener for a property.
            ///
            /// @param[in] begin The initial value to set to the property.
            /// @param[in] end The final value to set to the property.
            /// @param[in] duration The tweener's duration in secconds.
            /// @param[in] setter The setter function to use to set the
            ///            property's value.
            /// @param[in] easing The easing function to use to compute the
            ///            intermediate values.
            ///
            /// @return The smart pointer to the created Simple tweener.
            ///
            static Ptr New (double begin, double end, double duration,
                    Setter setter, EasingFunction easing) {
                return Ptr (new Simple (begin, end, duration, setter, easing));
            }

            ///
            /// @brief Creates a new Simple tweener for a value.
            ///
            /// @param[in] value The reference to the value to tween.  The
            ///                  current value of this reference is used as the
            ///                  starting value.
            /// @param[in] end The final value to set.
            /// @param[in] duration The tweener's duration in seconds.
            /// @param[in] easing The easing function to use to compute the
            ///            intermediate values.
            ///
            /// @return The smart pointer to the created Simple tweener.
            ///
            static Ptr New (double &value, double end, double duration,
                    EasingFunction easing) {
                return Ptr (new Simple (value, end, duration, easing));
            }

            ///
            /// @brief Creates a new Simple tweener for a value.
            ///
            /// @param[in] value The reference to the value to tween.
            /// @param[in] begin The initial value to set to.
            /// @param[in] end The final value to set to.
            /// @param[in] duration The tweener's duration in seconds.
            /// @param[in] easing The easing function to use to compute the
            ///            intermediate values.
            ///
            /// @return The smart pointer to the created Simple tweener.
            ///
            static Ptr New (double &value, double begin, double end,
                    double duration, EasingFunction easing) {
                return Ptr (new Simple (value, begin, end, duration, easing));
            }

            virtual bool isFinished () const;

            ///
            /// @brief Sets the tweener's begin value.
            ///
            /// @param[in] begin The begin value for the Tweener.
            ///
            void setBegin (double begin) { this->begin = begin; }

        protected:
            ///
            /// @brief Constructor for a property.
            ///
            /// @param[in] begin The initial value to set to the property.
            /// @param[in] end The final value to set to the property.
            /// @param[in] duration The tweener's duration in secconds.
            /// @param[in] setter The setter function to use to set the
            ///            property's value.
            /// @param[in] easing The easing function to use to compute the
            ///            intermediate values.
            ///
            Simple (double begin, double end, double duration, Setter setter,
                    EasingFunction easing);

            ///
            /// @brief Constructor for a value.
            ///
            /// @param[in] value The reference to the value to tween.  The
            ///                  current value of this reference is used as the
            ///                  starting value.
            /// @param[in] end The final value to set.
            /// @param[in] duration The tweener's duration in seconds.
            /// @param[in] easing The easing function to use to compute the
            ///            intermediate values.
            ///
            Simple (double &value, double end, double duration,
                    EasingFunction easing);

            ///
            /// @brief Constructor for a value.
            ///
            /// @param[in] value The reference to the value to tween.
            /// @param[in] begin The initial value to set to.
            /// @param[in] end The final value to set to.
            /// @param[in] duration The tweener's duration in seconds.
            /// @param[in] easing The easing function to use to compute the
            ///            intermediate values.
            ///
            Simple (double &value, double begin, double end, double duration,
                    EasingFunction easing);

            virtual void doUpdate (double elapsed);

        private:
            /// The property's initial value.
            double begin;
            /// The current tweener's time.
            double current;
            /// The tweener's duration.
            double duration;
            /// The easing function to use to compute intermediate values.
            EasingFunction easing;
            /// The property's final value.
            double end;
            /// The setter to use to update the property.
            Setter setter;
    };
} }

#endif // !GEISHA_STUDIOS_BENZAITEN_TWEENERS_SIMPLE_HPP
