//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "Simple.hpp"
#include <algorithm>
#include <boost/bind.hpp>

using namespace benzaiten::tweener;

Simple::Simple (double begin, double end, double duration, Setter setter,
        EasingFunction easing)
    : Tweener ()
    , begin (begin)
    , current (0.0)
    , duration (duration)
    , easing (easing)
    , end (end)
    , setter (setter)
{
}

Simple::Simple (double &value, double end, double duration,
        EasingFunction easing)
    : Tweener ()
    , begin (value)
    , current (0.0)
    , duration (duration)
    , easing (easing)
    , end (end)
    , setter ([&value](double v) {value = v; })
{
}

Simple::Simple (double &value, double begin, double end, double duration,
        EasingFunction easing)
    : Tweener ()
    , begin (begin)
    , current (0.0)
    , duration (duration)
    , easing (easing)
    , end (end)
    , setter ([&value](double v) { value = v; })
{
}

void Simple::doUpdate (double elapsed) {
    const double time = std::min (this->duration - this->current, elapsed);
    this->current += time;
    const double dt = this->easing (this->current / this->duration);
    const double value = this->begin + dt * (this->end - this->begin);
    this->setter (value);
}

bool Simple::isFinished () const {
    return this->current >= this->duration;
}
