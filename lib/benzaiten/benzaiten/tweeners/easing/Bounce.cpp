//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "Bounce.hpp"

namespace benzaiten { namespace easing {
    double easingBounceFunction (double time) {
        const double v = 1 - time;
        double c;
        double d;

        if (v < (1 / 2.75)) {
            c = v;
            d = 0;
        } else if (v < (2.0 / 2.75)) {
            c = v - 1.5 / 2.75;
            d = 0.75;
        } else if (v < (2.5 / 2.75)) {
            c = v - 2.25 / 2.75;
            d = 0.9375;
        } else {
            c = v - 2.625 / 2.75;
            d = 0.984375;
        }
        return 1 - (7.5625 * c * c + d);
    }
} }
