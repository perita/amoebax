//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_FONT_HPP)
#define GEISHA_STUDIOS_BENZAITEN_FONT_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <boost/shared_ptr.hpp>
#include "SDL.hpp"

namespace benzaiten {
    ///
    /// @class Font
    /// @brief The base class for fonts implementations.
    ///
    class Font {
        public:
            typedef boost::shared_ptr<Font> Ptr;

            ///
            /// @brief Destructor.
            ///
            virtual ~Font () { }

            ///
            /// @brief Renders a string to a texture.
            ///
            /// @param[in] text The text to write.
            ///
            /// @return The texture with the text rendered.
            ///
            virtual SDL::Texture renderText (const std::string &text) const = 0;

            ///
            /// @brief Gets the height that a text will have.
            ///
            /// @param[in] text The text to get its height.
            /// @return The font's height.
            ///
            virtual int height () const = 0;

            ///
            /// @brief Gets the width that a text will have.
            ///
            /// @param[in] text The text to get its width.
            /// @return The width, in pixels, that @p text will have.
            ///
            virtual int width (const std::string &text) const = 0;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_FONT_HPP
