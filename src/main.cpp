//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include <cstdlib>
#include <ctime>
#include <stdexcept>
#include <benzaiten/Game.hpp>
#include <benzaiten/ErrorMessage.hpp>
#include <benzaiten/contexts/Logo.hpp>
#include <boost/assign.hpp>
#include "contexts/Preload.hpp"

namespace amoebax {
    class Main: public benzaiten::Game {
        public:
            Main ()
                : Game (400, 240, "Amoebax", "amoebax", true)
            {
#if defined (EMSCRIPTEN)
                this->setScreenFillPolicy (benzaiten::Screen::FILL_BOTH);
#else
                setWindowScale (2);
#endif // !EMSCRIPTEN
            }

        protected:
            virtual void init () {
                using benzaiten::contexts::Logo;
                using boost::assign::list_of;

                std::srand (std::time (0));

                Game::input ().mapKeys (
                        Action::DOWN, list_of<int>(SDLK_DOWN)(SDLK_j));
                Game::input ().mapKeys (
                        Action::LEFT, list_of<int>(SDLK_LEFT)(SDLK_h));
                Game::input ().mapKeys (
                        Action::RIGHT, list_of<int>(SDLK_RIGHT)(SDLK_l));
                Game::input ().mapKeys (
                        Action::ROTATE, list_of<int>(SDLK_UP)(SDLK_k));

                switchTo (Logo::New ("logo.tga", Preload::New ()));
            }
    };
}

int main (int argc, char *argv[]) {
    amoebax::Main game;
    try {
        game ();
        return EXIT_SUCCESS;
    } catch (std::exception &e) {
        benzaiten::showErrorMessage (e.what ());
    } catch (...) {
        benzaiten::showUnknownErrorMessage ();
    }
    return EXIT_FAILURE;
}
