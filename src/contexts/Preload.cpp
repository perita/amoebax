//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Preload.hpp"
#include <benzaiten/Game.hpp>
#include <benzaiten/contexts/FadeTo.hpp>
#include <benzaiten/graphics/Text.hpp>
#include "MainMenu.hpp"
#include "../Font.hpp"

using namespace amoebax;
using namespace benzaiten;

Preload::Preload ()
    : Context ()
    , musics_to_load ()
    , sounds_to_load ()
    , textures_to_load ()
{
    this->musics_to_load.push_back ("menu.ogg");
    this->musics_to_load.push_back ("training.ogg");

    this->sounds_to_load.push_back ("awww.ogg");
    this->sounds_to_load.push_back ("cheer.ogg");
    this->sounds_to_load.push_back ("go.ogg");
    this->sounds_to_load.push_back ("levelup.ogg");
    this->sounds_to_load.push_back ("pop.ogg");
    this->sounds_to_load.push_back ("ready.ogg");

    this->textures_to_load.push_back ("amoebas.tga");
    this->textures_to_load.push_back ("amoebas-silhouette.tga");
    this->textures_to_load.push_back ("background-menu.tga");
    this->textures_to_load.push_back ("characters.tga");
    this->textures_to_load.push_back ("grid-training.tga");
    this->textures_to_load.push_back ("grid-twogrids.tga");
    this->textures_to_load.push_back ("orb.tga");
    this->textures_to_load.push_back ("readygo.tga");
    this->textures_to_load.push_back ("title-amoebax.tga");
}

void Preload::begin () {
    Text::Ptr text (Text::New (Font::menu (), "Loading..."));
    text->centerOrigin ();
    this->addGraphic (text, Game::screen ().half_width,
            Game::screen ().half_height);
}

void Preload::update (double elapsed) {
    Context::update (elapsed);

    if (!this->musics_to_load.empty ()) {
        std::list<std::string>::iterator music = this->musics_to_load.begin ();
        Game::resources ().cache (Game::resources ().try_music (*music));
        music = this->musics_to_load.erase (music);
        return;
    }

    if (!this->sounds_to_load.empty ()) {
        std::list<std::string>::iterator sound = this->sounds_to_load.begin ();
        Game::resources ().cache (Game::resources ().try_sound (*sound));
        sound = this->sounds_to_load.erase (sound);
        return;
    }

    if (!this->textures_to_load.empty ()) {
        std::list<std::string>::iterator texture = this->textures_to_load.begin ();
        Game::resources ().cache (Game::resources ().texture (*texture));
        texture = this->textures_to_load.erase (texture);
        if (texture == this->textures_to_load.end ()) {
            Game::switchTo (contexts::FadeTo::New (MainMenu::New ()));
        }
    }
}
