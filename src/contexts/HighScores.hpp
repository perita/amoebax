//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_CONTEXTS_HIGH_SCORES_HPP)
#define GEISHA_STUDIOS_AMOEBAX_CONTEXTS_HIGH_SCORES_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include <benzaiten/Context.hpp>
#include <benzaiten/Music.hpp>
#include <benzaiten/Sprite.hpp>
#include <boost/shared_ptr.hpp>

namespace amoebax {
    class Settings;

    class HighScores: public benzaiten::Context {
        public:
            typedef boost::shared_ptr<Context> Ptr;

            enum Page {
                NORMAL = 0,
                TRAINING
            };

            static Ptr New (unsigned int score, Page page) {
                return Ptr (new HighScores (score, page));
            }

            virtual void begin ();
            virtual void update (double elapsed);

        protected:
            HighScores (unsigned int score, Page page);

        private:
            void createPage (int offset_x, Settings &settings,
                    const std::string &page_name, unsigned int new_score,
                    bool update);

            std::vector<benzaiten::Sprite::Ptr> labels;
            int mouse_dir;
            int mouse_x;
            benzaiten::Music::Ptr music;
            benzaiten::Context::Ptr next;
            Page page;
            benzaiten::Sprite::Ptr subtitle_normal;
            benzaiten::Sprite::Ptr subtitle_training;
            benzaiten::Sprite::Ptr title;
    };
}

#endif // !GEISHA_STUDIOS_AMOEBAX_CONTEXTS_HIGH_SCORES_HPP
