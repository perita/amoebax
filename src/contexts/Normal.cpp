//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Normal.hpp"
#include <cassert>
#include <benzaiten/Game.hpp>
#include <benzaiten/contexts/FadeTo.hpp>
#include <benzaiten/tweeners/Sequence.hpp>
#include <benzaiten/tweeners/Simple.hpp>
#include <benzaiten/tweeners/easing/Linear.hpp>
#include <boost/assign.hpp>
#include <boost/bind.hpp>
#include "End.hpp"
#include "Pause.hpp"
#include "TwoGrids.hpp"
#include "../Font.hpp"
#include "../players/Human.hpp"
#include "../players/Shitty.hpp"
#include "../players/Simple.hpp"

using namespace amoebax;
using namespace benzaiten;

namespace {
    const double OUTSIDE = 60;
    const double OPPONENT_POS = 100;
    const double PLAYER_POS = 300;
    const double TWEENERS_DURATION = 0.25;
}

Normal::Normal (const std::string &player)
    : Context ()
    , current_theme ()
    , font (Font::menu ())
    , music (Game::resources ().try_music ("menu.ogg"))
    , opponent ()
    , player_name (player)
    , score (0)
    , themes ()
    , versus ()
{
    this->addGraphic (Game::resources ().texture ("background-menu.tga"),
            0, 0, -1);

    TextureAtlas::Ptr player_atlas = Game::resources ().atlas ("characters.xml");
    player_atlas->play (player);
    player_atlas->centerOrigin ();
    this->addGraphic (player_atlas, PLAYER_POS, Game::screen ().half_height);

    this->opponent.atlas = Game::resources ().atlas ("characters.xml");
    this->opponent.sprite = this->addGraphic (this->opponent.atlas, -OUTSIDE,
            Game::screen ().half_height);

    Texture::Ptr versus_texture = Texture::New (
            Game::resources ().texture ("versus.tga"));
    versus_texture->centerOrigin ();
    this->versus = this->addGraphic (versus_texture, Game::screen ().half_width,
            Game::screen ().height + OUTSIDE);

    boost::assign::push_back(this->themes)
        ("kquita", boost::bind(&player::Shitty::New))
        ("angus", boost::bind(&player::Simple::New, 0, false))
        ("catdog", boost::bind(&player::Simple::New, 0, true))
        ("spike", boost::bind(&player::Simple::New, 1, true))
        ("mrbones", boost::bind(&player::Simple::New, 2, true))
        ("pen", boost::bind(&player::Simple::New, 2, true)) // TODO: Correct player type
        ;
    this->current_theme = this->themes.begin ();
}

void Normal::begin () {
    this->opponent.sprite->moveToX (-OUTSIDE);
    if (this->current_theme != this->themes.end ()) {
        this->opponent.set (this->current_theme->name);
        tweener::Sequence::Ptr tweeners = tweener::Sequence::New ();
        tweeners->add (tweener::Simple::New (Game::screen ().height + OUTSIDE,
                    Game::screen ().half_height, TWEENERS_DURATION,
                    boost::bind (&Sprite::moveToY, this->versus.get (), _1),
                    easing::Linear::easeIn));
        tweeners->add (tweener::Simple::New (-OUTSIDE, OPPONENT_POS,
                    TWEENERS_DURATION,
                    boost::bind (&Sprite::moveToX, this->opponent.sprite.get (), _1),
                    easing::Linear::easeIn));
        tweeners->setFinishCallback (boost::bind (&Normal::startGame, this));
        this->addTweener (tweeners);
    } else {
        Game::switchTo (End::New (this->player_name, this->score));
    }
}

void Normal::onFull (TwoGrids &grids, TwoGridsObserver::Side side) {
    if (side == TwoGridsObserver::LEFT) {
        this->current_theme++;
        this->score += grids.getRightScore ();
    }
}

void Normal::onPause (TwoGrids &grids) {
    Game::switchTo (Pause::New (this->score, HighScores::NORMAL));
}

void Normal::startGame () {
    assert (this->current_theme != this->themes.end ());

    this->music->halt ();
    Game::switchTo (contexts::FadeTo::New (
            TwoGrids::New (this, this->current_theme->name,
                this->current_theme->player (), 0,
                player::Human::New (), this->score)));
}
