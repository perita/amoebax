//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_CONTEXTS_NORMAL_HPP)
#define GEISHA_STUDIOS_AMOEBAX_CONTEXTS_NORMAL_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include <benzaiten/Context.hpp>
#include <benzaiten/Font.hpp>
#include <benzaiten/Music.hpp>
#include <benzaiten/graphics/TextureAtlas.hpp>
#include <boost/shared_ptr.hpp>
#include "../interfaces/TwoGridsObserver.hpp"
#include "../sprites/Player.hpp"

namespace amoebax {
    class Normal: public benzaiten::Context, TwoGridsObserver {
        public:
            typedef boost::shared_ptr<Normal> Ptr;

            static Ptr New (const std::string &player) {
                return Ptr (new Normal (player));
            }

            virtual void begin ();
            virtual void onFull (TwoGrids &grids, TwoGridsObserver::Side side);
            virtual void onPause (TwoGrids &grids);

        protected:
            Normal (const std::string &player);

        private:
            struct Theme {
                typedef boost::function <Player::Ptr ()> PlayerFun;

                std::string name;
                PlayerFun player;

                Theme (const std::string &name, PlayerFun player)
                    : name (name)
                    , player (player)
                {
                }
            };

            struct Opponent {
                benzaiten::TextureAtlas::Ptr atlas;
                benzaiten::Sprite::Ptr sprite;

                void set (const std::string &name) {
                    this->atlas->play (name);
                    this->atlas->centerOrigin ();
                }
            };

            void startGame ();

            std::vector<Theme>::iterator current_theme;
            benzaiten::Font::Ptr font;
            benzaiten::Music::Ptr music;
            Opponent opponent;
            const std::string player_name;
            unsigned int score;
            std::vector<Theme> themes;
            benzaiten::Sprite::Ptr versus;
    };
}

#endif // !GEISHA_STUDIOS_AMOEBAX_CONTEXTS_NORMAL_HPP
