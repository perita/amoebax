//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "TwoGrids.hpp"
#include <benzaiten/Game.hpp>
#include <benzaiten/SoundNull.hpp>
#include <benzaiten/VirtualJoystick.hpp>
#include <boost/bind.hpp>
#include <boost/format.hpp>
#include "../Grid.hpp"
#include "../sprites/Orb.hpp"
#include "../sprites/ReadyGo.hpp"
#include "../sprites/Sign.hpp"

using namespace amoebax;
using namespace benzaiten;

namespace {
    const int LEFT_GRID_X = 52;
    const int LEFT_GRID_Y = 12;
    const int LEFT_SCORE_X = LEFT_GRID_X + 165;
    const int LEFT_SCORE_Y = LEFT_GRID_Y + 205;
    const int LEFT_SIGN_X = LEFT_GRID_X + 48;
    const int LEFT_SIGN_Y = LEFT_GRID_Y + 101;
    const int RIGHT_GRID_X = LEFT_GRID_X + 184;
    const int RIGHT_GRID_Y = LEFT_GRID_Y;
    const int RIGHT_SCORE_X = RIGHT_GRID_X - 5;
    const int RIGHT_SCORE_Y = RIGHT_GRID_Y + 175;
    const int RIGHT_SIGN_X = RIGHT_GRID_X + 48;
    const int RIGHT_SIGN_Y = RIGHT_GRID_Y + 101;
}

TwoGrids::TwoGrids (TwoGridsObserver *observer, const std::string &theme,
        const Player::Ptr &left_player, unsigned int left_score,
        const Player::Ptr &right_player, unsigned int right_score)
    : Context ()
    , music (Game::resources ().try_music (
                boost::str(boost::format ("%1%.ogg") % theme)))
    , observer (observer)
    , player_left (left_player)
    , player_right (right_player)
    , playing (true)
    , prev (Game::activeContext ())
    , show_ready_go (true)
{

    this->addGraphic (Game::resources ().texture ("grid-twogrids.tga"),
            LEFT_GRID_X, LEFT_GRID_Y - 1, -1);
    this->addGraphic (Game::resources ().texture (
                boost::str (boost::format ("background-%1%.tga") % theme)),
            0, 0, -1);
    this->player_left->setGrid (Grid::New (*this, LEFT_GRID_X, LEFT_GRID_Y,
                LEFT_SCORE_X, LEFT_SCORE_Y, true, left_score));
    this->add (this->player_left);
    this->player_right->setGrid (Grid::New (*this, RIGHT_GRID_X, RIGHT_GRID_Y,
                RIGHT_SCORE_X, RIGHT_SCORE_Y, false, right_score));
    this->add (this->player_right);
}

void TwoGrids::begin () {
    if (this->show_ready_go) {
        this->add (ReadyGo::New (LEFT_SIGN_X, LEFT_SIGN_Y));
        this->add (ReadyGo::New (RIGHT_SIGN_X, RIGHT_SIGN_Y,
                    boost::bind (&TwoGrids::startGame, this)));
        this->show_ready_go = false;
    }

    if (Music::isPaused ()) {
        this->music->resume ();
    }
}

unsigned int TwoGrids::getLeftScore () const {
    return this->player_left->getGrid ().getScore ();
}

unsigned int TwoGrids::getRightScore () const {
    return this->player_right->getGrid ().getScore ();
}

void TwoGrids::end () {
    if (Music::isPlaying ()) {
        this->music->pause ();
    }
}

void TwoGrids::onActivePair (Grid &grid) {
    // Nothing to do.
}

void TwoGrids::onFull (Grid &grid) {
    this->music->halt ();
    this->player_left->setActive (false);
    this->player_right->setActive (false);
    this->playing = false;
    Game::input ().removeVirtualJoystick ();

    TwoGridsObserver::Side side;
    if (this->player_left->isHerGrid (grid)) {
        side = TwoGridsObserver::LEFT;
        this->add (Sign::New (Sign::LOSE, LEFT_SIGN_X, -80, LEFT_SIGN_Y,
                    Game::resources ().try_sound ("cheer.ogg")));
        this->add (Sign::New (Sign::WIN, RIGHT_SIGN_X,
                    Game::screen ().height + 80, RIGHT_SIGN_Y, SoundNull::New ()));
    } else {
        side = TwoGridsObserver::RIGHT;
        this->add (Sign::New (Sign::WIN, LEFT_SIGN_X,
                    Game::screen ().height + 80, LEFT_SIGN_Y,
                    Game::resources ().try_sound ("awww.ogg")));
        this->add (Sign::New (Sign::LOSE, RIGHT_SIGN_X, -80, RIGHT_SIGN_Y,
                    SoundNull::New ()));
    }
    if (this->observer) {
        this->observer->onFull (*this, side);
    }
}

void TwoGrids::onGhosts (Grid &grid, double x, double y, int to_remove,
        int to_add) {
    Grid *my_grid = 0;
    double my_x = 0.0;
    double my_y = 0.0;
    Grid *opponent_grid = 0;
    double opponent_x = 0.0;
    double opponent_y = 0.0;
    if (this->player_left->isHerGrid (grid)) {
        my_grid = &(this->player_left->getGrid ());
        my_x = LEFT_GRID_X + 50;
        my_y = LEFT_GRID_Y + 8;
        opponent_grid = &(this->player_right->getGrid ());
        opponent_x = RIGHT_GRID_X + 50;
        opponent_y = RIGHT_GRID_Y + 8;
    } else {
        my_grid = &(this->player_right->getGrid ());
        my_x = RIGHT_GRID_X + 50;
        my_y = RIGHT_GRID_Y + 8;
        opponent_grid = &(this->player_left->getGrid ());
        opponent_x = LEFT_GRID_X + 50;
        opponent_y = LEFT_GRID_Y + 8;
    }

    Orb::Ptr orb (Orb::New (x, y));
    if (to_remove > 0) {
        orb->addDestination (my_x, my_y,
                boost::bind (&Grid::removeGhosts, my_grid, to_remove));
    }
    if (to_add > 0) {
        orb->addDestination (opponent_x, opponent_y,
                boost::bind (&Grid::addGhosts, opponent_grid, to_add, true));
    }
    this->add (orb);
}

void TwoGrids::startGame () {
    this->music->play ();
    this->player_left->setActive (true);
    this->player_right->setActive (true);
}

void TwoGrids::update (double elapsed) {
    if (this->playing) {
        if (Game::input ().pressed (benzaiten::Action::MENU_CANCEL)) {
            if (this->observer) {
                this->observer->onPause (*this);
            } else {
                Game::switchTo (this->prev);
            }
        }
    } else {
        if (Game::input ().pressed (benzaiten::Action::MENU_CANCEL) ||
                Game::input ().mouse_down) {
            Game::switchTo (this->prev);
        }
    }
    Context::update (elapsed);
}
