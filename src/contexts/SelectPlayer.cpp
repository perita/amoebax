//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "SelectPlayer.hpp"
#include <benzaiten/Game.hpp>
#include <benzaiten/graphics/Texture.hpp>
#include <benzaiten/graphics/TextureAtlas.hpp>
#include <benzaiten/tweeners/Group.hpp>
#include <benzaiten/tweeners/Sequence.hpp>
#include <benzaiten/tweeners/Simple.hpp>
#include <benzaiten/tweeners/easing/Linear.hpp>
#include <boost/bind.hpp>
#include "MainMenu.hpp"
#include "Normal.hpp"

using namespace amoebax;
using namespace benzaiten;

namespace {
    const double KIM_POS = 300;
    const double OUTSIDE = 30;
    const double PADDING = 20;
    const double TITLE_BEGIN = -30;
    const double TITLE_END = 30;
    const double TOM_POS = 100;
    const double TWEENERS_DURATION = 0.5;
}

SelectPlayer::SelectPlayer ()
    : Context ()
    , kim ()
    , music (Game::resources ().try_music ("menu.ogg"))
    , selected (false)
    , title ()
    , tom ()
{
    this->addGraphic (Game::resources ().texture ("background-menu.tga"),
            0, 0, -1);
    Texture::Ptr title_texture (Texture::New (
                Game::resources ().texture ("title-select.tga")));
    title_texture->centerOrigin ();
    this->title = this->addGraphic (title_texture,
            Game::screen ().half_width, TITLE_BEGIN);

    TextureAtlas::Ptr kim_atlas = Game::resources ().atlas ("characters.xml");
    kim_atlas->play ("kim");
    kim_atlas->centerOrigin ();
    this->kim = this->addGraphic (kim_atlas, Game::screen ().width + OUTSIDE,
            Game::screen ().half_height);
    this->kim->setHitbox (PADDING + kim_atlas->width () / 2,
            PADDING + kim_atlas->height () / 2,
            kim_atlas->width () + PADDING * 2,
            kim_atlas->height () + PADDING * 2);

    TextureAtlas::Ptr tom_atlas = Game::resources ().atlas ("characters.xml");
    tom_atlas->play ("tom");
    tom_atlas->centerOrigin ();
    this->tom = this->addGraphic (tom_atlas, -OUTSIDE,
            Game::screen ().half_height);
    this->tom->setHitbox (PADDING + tom_atlas->width () / 2,
            PADDING + tom_atlas->height () / 2,
            tom_atlas->width () + PADDING * 2,
            tom_atlas->height () + PADDING * 2);
    this->addTweener (tweener::Simple::New (TITLE_BEGIN, TITLE_END,
                TWEENERS_DURATION,
                boost::bind (&Sprite::moveToY, this->title.get (), _1),
                easing::Linear::easeIn));
}

void SelectPlayer::begin () {
    tweener::Sequence::Ptr chars = tweener::Sequence::New ();
    chars->add (tweener::Simple::New (this->kim->x (), KIM_POS,
                TWEENERS_DURATION / 2,
                boost::bind (&Sprite::moveToX, this->kim.get (), _1),
                easing::Linear::easeIn));
    chars->add (tweener::Simple::New (this->tom->x (), TOM_POS,
                TWEENERS_DURATION / 2,
                boost::bind (&Sprite::moveToX, this->tom.get (), _1),
                easing::Linear::easeIn));
    this->addTweener (chars);
}

Tweener::Ptr SelectPlayer::removeTitle () {
    return tweener::Simple::New (TITLE_END, TITLE_BEGIN,
            TWEENERS_DURATION / 2,
            boost::bind (&Sprite::moveToY, this->title.get(), _1),
            easing::Linear::easeIn);
}

void SelectPlayer::returnToMainMenu () {
    Tweener::Ptr tweener = this->removeTitle ();
    tweener->setFinishCallback (boost::bind (&Game::switchTo,
                MainMenu::New ()));
    this->addTweener (tweener);
}

void SelectPlayer::selectKim () {
    tweener::Group::Ptr tweeners = tweener::Group::New ();
    tweeners->setFinishCallback (boost::bind (&Game::switchTo,
                Normal::New ("kim")));
    tweeners->add (this->removeTitle ());
    tweeners->add (tweener::Simple::New (this->tom->y (),
                Game::screen ().height + OUTSIDE * 2, TWEENERS_DURATION / 2,
                boost::bind (&Sprite::moveToY, this->tom.get (), _1),
                easing::Linear::easeIn));
    this->addTweener (tweeners);
}

void SelectPlayer::selectTom () {
    tweener::Group::Ptr tweeners = tweener::Group::New ();
    tweeners->setFinishCallback (boost::bind (&Game::switchTo,
                Normal::New ("tom")));
    tweeners->add (this->removeTitle ());

    tweener::Sequence::Ptr chars = tweener::Sequence::New ();
    chars->add (tweener::Simple::New (this->kim->y (),
                Game::screen ().height + OUTSIDE * 2, TWEENERS_DURATION / 4,
                boost::bind (&Sprite::moveToY, this->kim.get (), _1),
                easing::Linear::easeIn));
    chars->add (tweener::Simple::New (this->tom->x (),
                KIM_POS, TWEENERS_DURATION / 4,
                boost::bind (&Sprite::moveToX, this->tom.get (), _1),
                easing::Linear::easeIn));
    tweeners->add (chars);

    this->addTweener (tweeners);
}

void SelectPlayer::update (double elapsed) {
    Context::update (elapsed);
    if (!this->selected) {
        if (Game::input ().pressed (benzaiten::Action::MENU_CANCEL)) {
            this->selected = true;
            this->returnToMainMenu ();
        } else if (Game::input ().mouse_pressed) {
            Point2D<double> pos (Game::input ().mouse_x, Game::input ().mouse_y);
            if (this->kim->collides (pos)) {
                this->selected = true;
                this->selectKim ();
            } else if (this->tom->collides (pos)) {
                this->selected = true;
                this->selectTom ();
            }
        }
    }
}
