//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Training.hpp"
#include <benzaiten/Game.hpp>
#include <benzaiten/VirtualJoystick.hpp>
#include <benzaiten/graphics/TextureAtlas.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/bind.hpp>
#include "HighScores.hpp"
#include "Pause.hpp"
#include "../Font.hpp"
#include "../players/Human.hpp"
#include "../sprites/ReadyGo.hpp"
#include "../sprites/LevelUp.hpp"
#include "../sprites/Orb.hpp"
#include "../sprites/Sign.hpp"

using namespace amoebax;
using namespace benzaiten;

namespace {
    const int GRID_POS_X = 153;
    const int GRID_POS_Y = 14;
    const int SCORE_POS_X = GRID_POS_X + 167;
    const int SCORE_POS_Y = GRID_POS_Y + 172;
    const int LEVEL_POS_X = GRID_POS_X + 167;
    const int LEVEL_POS_Y = GRID_POS_Y + 200;
}

Training::Training ()
    : Context ()
    , current_level (0)
    , level_label (Text::New (Font::score (), "1"))
    , levels ()
    , music (Game::resources ().try_music ("training.ogg"))
    , player (player::Human::New ())
    , playing (true)
{
    this->levels.reserve (16);
    this->levels.push_back (Level (    0,  0, 0));
    this->levels.push_back (Level (  440, 20, 1));
    this->levels.push_back (Level ( 1240, 25, 1));
    this->levels.push_back (Level ( 2200, 30, 1));
    this->levels.push_back (Level ( 3712, 30, 2));
    this->levels.push_back (Level ( 5896, 40, 1));
    this->levels.push_back (Level ( 8080, 40, 2));
    this->levels.push_back (Level (10000, 45, 1));
    this->levels.push_back (Level (10500, 30, 3));
    this->levels.push_back (Level (13000, 50, 2));
    this->levels.push_back (Level (16000, 35, 4));
    this->levels.push_back (Level (18500, 40, 4));
    this->levels.push_back (Level (21000, 45, 4));
    this->levels.push_back (Level (23500, 40, 5));
    this->levels.push_back (Level (26000, 60, 6));

    this->addGraphic (Game::resources ().texture ("grid-training.tga"),
            GRID_POS_X, GRID_POS_Y, -1);
    this->addGraphic (Game::resources ().texture ("background-menu.tga"),
            0, 0, -1);
    this->add (ReadyGo::New (Game::screen ().half_width,
                Game::screen ().half_height,
                boost::bind (&Training::startGame, this)));

    this->level_label->alignRight ();
    this->addGraphic (this->level_label, LEVEL_POS_X, LEVEL_POS_Y, 10);

    this->player->setGrid (Grid::New (*this, GRID_POS_X, GRID_POS_Y,
                SCORE_POS_X, SCORE_POS_Y, true));
    this->add (player);
}

void Training::begin () {
    if (Music::isPaused ()) {
        this->music->resume ();
    }
}

void Training::end () {
    if (Music::isPlaying ()) {
        this->music->pause ();
    }
}

void Training::onActivePair (Grid &grid) {
    const Level &level (this->levels[this->current_level]);
    if (level.max_ghosts > 0 && (rand () % 101) < level.probability) {
        grid.addGhosts ((rand () % level.max_ghosts) + 1, false);
    }
    if (this->current_level < (this->levels.size () - 1) &&
            grid.getScore () > this->levels[this->current_level + 1].score) {
        ++this->current_level;
        this->level_label->setText (
                boost::lexical_cast<std::string>(this->current_level + 1));
        this->add (LevelUp::New (Game::screen ().half_width,
                    Game::screen ().half_height));
    }
}

void Training::onFull (Grid &grid) {
    this->music->halt ();
    this->add (Sign::New (Sign::GAMEOVER,
                Game::screen ().half_width, -80, Game::screen ().half_height,
                Game::resources ().try_sound ("awww.ogg")));
    Game::input ().removeVirtualJoystick ();
    this->player->setActive (false);
    this->playing = false;
}

void Training::onGhosts (Grid &grid, double x, double y, int to_remove,
        int to_add) {
    if (to_remove > 0) {
        Orb::Ptr orb (Orb::New (x, y));
        orb->addDestination (GRID_POS_X + 50, GRID_POS_Y + 8,
                boost::bind (&Grid::removeGhosts, &grid, to_remove));
        this->add (orb);
    }
}

void Training::startGame () {
    this->music->play ();
    this->player->setActive (true);
}

void Training::update (double elapsed) {
    Context::update (elapsed);
    if (this->playing) {
        if (Game::input ().pressed (benzaiten::Action::MENU_CANCEL)) {
            Game::switchTo (Pause::New (this->player->getGrid ().getScore (),
                        HighScores::TRAINING));
        }
    } else {
        if (Game::input ().pressed (benzaiten::Action::MENU_CANCEL) ||
                Game::input ().mouse_down) {
            Game::switchTo (HighScores::New (
                        this->player->getGrid ().getScore (),
                        HighScores::TRAINING));
        }
    }
}
