//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "MainMenu.hpp"
#include <benzaiten/Game.hpp>
#include <benzaiten/graphics/Texture.hpp>
#include <benzaiten/tweeners/Group.hpp>
#include <benzaiten/tweeners/Simple.hpp>
#include <benzaiten/tweeners/easing/Back.hpp>
#include <benzaiten/tweeners/easing/Linear.hpp>
#include <boost/assign.hpp>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include "Credits.hpp"
#include "HighScores.hpp"
#include "SelectPlayer.hpp"
#include "Training.hpp"
#include "../Font.hpp"
#include "../sprites/MenuOption.hpp"

using namespace amoebax;
using namespace benzaiten;

namespace {
    const double MENU_FIRST_ROW = 80;
    const double MENU_ROW_HEIGHT = 40;
    const double TITLE_BEGIN = -25;
    const double TITLE_END = 30;
    const double TWEENERS_DURATION = 0.5;
    const double TWEENERS_DELAY = 0.05;

    struct Option {
        enum Id {
            Credits,
            Exit,
            HighScores,
            Normal,
            Training
        };
        const Id id;
        const std::string label;

        Option (Id id, const std::string &label)
            : id (id)
            , label (label)
        {
        }
    };
}

MainMenu::MainMenu ()
    : Context ()
    , font (Font::menu ())
    , music (Game::resources ().try_music ("menu.ogg"))
    , next ()
    , options ()
    , title ()
{
    this->addGraphic (Game::resources ().texture ("background-menu.tga"),
            0, 0, -1);

    Texture::Ptr title_texture (Texture::New (
                Game::resources ().texture ("title-amoebax.tga")));
    title_texture->centerOrigin ();
    this->title = this->addGraphic (title_texture,
            Game::screen ().half_width, TITLE_BEGIN);
}

void MainMenu::begin () {
    Game::camera ().x = 0.0;

    this->addTweener (tweener::Simple::New (TITLE_BEGIN, TITLE_END,
                TWEENERS_DURATION,
                boost::bind (&Sprite::moveToY, this->title.get (), _1),
                easing::Linear::easeIn));

    using boost::assign::list_of;
    const std::list<Option> options =
        list_of (Option (Option::Training, "training"))
                (Option (Option::Normal, "normal"))
                (Option (Option::HighScores, "high scores"))
                (Option (Option::Credits, "credits"));
    unsigned int option_index = 0;
    BOOST_FOREACH (const Option &option, options) {
        Sprite::Ptr sprite = this->add (MenuOption::New (*this,
                        option.id, Game::screen ().width + 1,
                        MENU_FIRST_ROW + MENU_ROW_HEIGHT * option_index, font,
                        option.label));
        this->options.push_back (sprite);
        this->addTweener (tweener::Simple::New (sprite->x (),
                    Game::screen ().half_width, TWEENERS_DURATION,
                    boost::bind (&Sprite::moveToX, sprite.get (), _1),
                    easing::Back::easeOut))->
            setDelay (option_index * TWEENERS_DELAY);
        ++option_index;
    }

    if (!Music::isPlaying () || Music::isPaused ()) {
        this->music->play ();
    }
}

void MainMenu::onOption (int option) {
    switch (option) {
        case Option::Credits:
            this->next = Credits::New ();
            break;

        case Option::HighScores:
            this->next = HighScores::New (0, HighScores::NORMAL);
            break;

        case Option::Exit:
            this->next = Context::Ptr (); // Quit.
            break;

        case Option::Normal:
            this->next = SelectPlayer::New ();
            break;

        case Option::Training:
            this->next = Training::New ();
            this->music->halt ();
            break;

        default:
            assert (false && option && "No valid menu option");
            break;
    }

    tweener::Group::Ptr tweeners = tweener::Group::New ();
    tweeners->add (tweener::Simple::New (TITLE_END, TITLE_BEGIN,
                TWEENERS_DURATION / 2,
                boost::bind (&Sprite::moveToY, this->title.get(), _1),
                easing::Linear::easeIn));
    unsigned int option_index = 0;
    BOOST_REVERSE_FOREACH (Sprite::Ptr &option, this->options) {
        tweeners->add (tweener::Simple::New (option->y (),
                    option->y() + Game::screen().height,
                    TWEENERS_DURATION / 2,
                    boost::bind (&Sprite::moveToY, option.get(), _1),
                    easing::Linear::easeIn))->
            setDelay (option_index * TWEENERS_DELAY);
        ++option_index;
    }
    tweeners->setFinishCallback ( boost::bind (&MainMenu::switchToNext, this));
    this->addTweener (tweeners);
}

void MainMenu::switchToNext () {
    Game::switchTo (this->next);
}

void MainMenu::update (double elapsed) {
    Context::update (elapsed);
    if (!this->next && Game::input ().pressed (benzaiten::Action::MENU_CANCEL)) {
        this->onOption (Option::Exit);
    }
}
