//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_CONTEXTS_PRELOAD_HPP)
#define GEISHA_STUDIOS_AMOEBAX_CONTEXTS_PRELOAD_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <list>
#include <string>
#include <benzaiten/Context.hpp>
#include <boost/shared_ptr.hpp>

namespace amoebax {
    class Preload: public benzaiten::Context {
        public:
            typedef boost::shared_ptr<Preload> Ptr;

            static Ptr New () { return Ptr (new Preload ()); }

            virtual void begin ();
            virtual void update (double elapsed);

        protected:
            Preload ();

        private:
            std::list<std::string> musics_to_load;
            std::list<std::string> sounds_to_load;
            std::list<std::string> textures_to_load;
    };
}

#endif // !GEISHA_STUDIOS_AMOEBAX_CONTEXTS_PRELOAD_HPP
