//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Credits.hpp"
#include <benzaiten/Game.hpp>
#include <benzaiten/graphics/Text.hpp>
#include <benzaiten/tweeners/Group.hpp>
#include <benzaiten/tweeners/Sequence.hpp>
#include <benzaiten/tweeners/Simple.hpp>
#include <benzaiten/tweeners/easing/Linear.hpp>
#include <boost/assign.hpp>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include "MainMenu.hpp"
#include "../Font.hpp"

using namespace amoebax;
using namespace benzaiten;

namespace {
    const double LABELS_Y = 80;
    const double LABELS_X_START = -140;
    const double LABELS_X_END = 20;
    const double LABELS_INDENT = 50;
    const double TITLE_BEGIN = -25;
    const double TITLE_END = 30;
    const double TWEENERS_DURATION = 0.5;
}

Credits::Credits ()
    : Context ()
    , font (Font::menu ())
    , labels ()
    , music (Game::resources ().try_music ("menu.ogg"))
    , next ()
    , title ()
{
    this->addGraphic (Game::resources ().texture ("background-menu.tga"),
            0, 0, -1);
    Texture::Ptr title_texture (Texture::New (
                Game::resources ().texture ("title-credits.tga")));
    title_texture->centerOrigin ();
    this->title = this->addGraphic (title_texture,
            Game::screen ().half_width, TITLE_BEGIN);
}

void Credits::begin () {
    this->addTweener (tweener::Simple::New (TITLE_BEGIN, TITLE_END,
                TWEENERS_DURATION,
                boost::bind (&Sprite::moveToY, this->title.get (), _1),
                easing::Linear::easeIn));

    tweener::Sequence::Ptr tweeners = tweener::Sequence::New ();
    std::vector<std::string> texts = boost::assign::list_of
        ("Graphics")("Safareig Creatiu")
        ("Music & Sound")("Alex Almarza")
        ("Programming")("Jordi Fita");
    bool name = false;
    double y = LABELS_Y;
    BOOST_FOREACH (const std::string &text, texts) {
        Text::Ptr label = Text::New (this->font, text);
        if (name) {
            label->setColorMod (178, 255, 0);
        }
        this->labels.push_back (this->addGraphic (label, LABELS_X_START, y));
        tweeners->add (tweener::Simple::New (LABELS_X_START,
                    LABELS_X_END + (name ? LABELS_INDENT : 0),
                    TWEENERS_DURATION,
                    boost::bind (&Sprite::moveToX, this->labels.back ().get (), _1),
                    easing::Linear::easeIn));
         y += this->font->height () * (name ? 1.75 : 1.25);
         name = !name;
    }
    this->addTweener (tweeners);
}

void Credits::switchToNext () {
    if (this->next) {
        Game::switchTo (this->next);
    }
}

void Credits::update (double elapsed) {
    Context::update (elapsed);
    if (!this->next &&
            (Game::input ().pressed (benzaiten::Action::MENU_CANCEL) ||
             Game::input ().mouse_pressed)) {
        this->next = MainMenu::New ();

        tweener::Group::Ptr tweeners = tweener::Group::New ();
        tweeners->add (tweener::Simple::New (SDL_ALPHA_OPAQUE,
                    SDL_ALPHA_TRANSPARENT,
                    TWEENERS_DURATION / 2,
                    boost::bind (&Sprite::setAlpha, this->title.get(), _1),
                    easing::Linear::easeIn));
        BOOST_FOREACH (Sprite::Ptr &label, this->labels) {
            tweeners->add (tweener::Simple::New (SDL_ALPHA_OPAQUE,
                        SDL_ALPHA_TRANSPARENT,
                        TWEENERS_DURATION / 2,
                        boost::bind (&Sprite::setAlpha, label.get(), _1),
                        easing::Linear::easeIn));
        }
        tweeners->setFinishCallback (boost::bind (&Credits::switchToNext, this));
        this->addTweener (tweeners);
    }
}
