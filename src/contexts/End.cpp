//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "End.hpp"
#include <benzaiten/Game.hpp>
#include <benzaiten/contexts/FadeTo.hpp>
#include <benzaiten/graphics/Texture.hpp>
#include <benzaiten/graphics/TextureAtlas.hpp>
#include <benzaiten/tweeners/Group.hpp>
#include <benzaiten/tweeners/Sequence.hpp>
#include <benzaiten/tweeners/Simple.hpp>
#include <benzaiten/tweeners/easing/Back.hpp>
#include <benzaiten/tweeners/easing/Linear.hpp>
#include <boost/bind.hpp>
#include "HighScores.hpp"

using namespace amoebax;
using namespace benzaiten;

namespace {
    const double PLAYER_X_BEGIN = 300;
    const double PLAYER_Y_FEET = 120;
    const double PODIUM_BEGIN = 100; // Relative to end of screen
    const double PODIUM_END = 180; // Absolute
    const double TITLE_BEGIN = -35;
    const double TITLE_END = 35;
    const double TWEENERS_DURATION = 0.5;
}

End::End (const std::string &player, unsigned int score)
    : Context ()
    , music (Game::resources ().try_music ("congratulations.ogg"))
    , player ()
    , player_height ()
    , score (score)
    , title ()
    , versus ()
{
    this->addGraphic (Game::resources ().texture ("background-menu.tga"),
            0, 0, -1);

    Texture::Ptr title_texture (Texture::New (
                Game::resources ().texture ("title-congratulations.tga")));
    title_texture->centerOrigin ();
    this->title = this->addGraphic (title_texture,
            Game::screen ().half_width, TITLE_BEGIN);

    TextureAtlas::Ptr player_atlas = Game::resources ().atlas ("characters.xml");
    player_atlas->play (player);
    player_atlas->centerOrigin ();
    this->player_height = player_atlas->height ();
    this->player = this->addGraphic (player_atlas, PLAYER_X_BEGIN,
            Game::screen ().half_height);

    Texture::Ptr versus_texture = Texture::New (
            Game::resources ().texture ("versus.tga"));
    versus_texture->centerOrigin ();
    this->versus = this->addGraphic (versus_texture, Game::screen ().half_width,
            Game::screen ().half_height);

    Texture::Ptr podium_texture (Texture::New (
                Game::resources ().texture ("podium.tga")));
    podium_texture->centerOrigin ();
    this->podium = this->addGraphic (podium_texture,
            Game::screen ().half_width, Game::screen ().height + PODIUM_BEGIN);
}

void End::begin () {
    tweener::Sequence::Ptr tweeners = tweener::Sequence::New ();

    tweeners->add (tweener::Simple::New (Game::screen ().half_height,
                Game::screen ().height * 1.5, TWEENERS_DURATION,
                boost::bind (&Sprite::moveToY, this->versus.get (), _1),
                easing::Linear::easeIn));

    tweener::Group::Ptr player_move = tweener::Group::New ();
    player_move->add (tweener::Simple::New (this->player->x (),
                Game::screen ().half_width, TWEENERS_DURATION,
                boost::bind (&Sprite::moveToX, this->player.get (), _1),
                easing::Linear::easeIn));
    player_move->add (tweener::Simple::New (this->player->y (),
                PLAYER_Y_FEET - this->player_height / 2, TWEENERS_DURATION,
                boost::bind (&Sprite::moveToY, this->player.get (), _1),
                easing::Linear::easeIn));
    tweeners->add (player_move);

    tweeners->add (tweener::Simple::New (TITLE_BEGIN, TITLE_END,
                TWEENERS_DURATION,
                boost::bind (&Sprite::moveToY, this->title.get (), _1),
                easing::Back::easeOut));

    tweeners->add (tweener::Simple::New (this->podium->y (), PODIUM_END,
                TWEENERS_DURATION,
                boost::bind (&Sprite::moveToY, this->podium.get (), _1),
                easing::Linear::easeIn));

    this->addTweener (tweeners);

    this->music->play ();
}

void End::update (double elapsed) {
    Context::update (elapsed);

    if (Game::input ().mouse_pressed ||
            Game::input ().pressed (benzaiten::Action::MENU_CANCEL)) {
        this->music->halt ();
        Game::switchTo (contexts::FadeTo::New (
                    HighScores::New (this->score, HighScores::NORMAL)));
    }
}
