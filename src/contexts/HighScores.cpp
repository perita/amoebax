//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "HighScores.hpp"
#include <benzaiten/Font.hpp>
#include <benzaiten/Game.hpp>
#include <benzaiten/Utils.hpp>
#include <benzaiten/graphics/GraphicList.hpp>
#include <benzaiten/graphics/Text.hpp>
#include <benzaiten/graphics/TileMap.hpp>
#include <benzaiten/tweeners/Group.hpp>
#include <benzaiten/tweeners/Simple.hpp>
#include <benzaiten/tweeners/easing/Linear.hpp>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include "MainMenu.hpp"
#include "../Font.hpp"
#include "../Settings.hpp"

using namespace amoebax;
using namespace benzaiten;

namespace {
    const double LABELS_Y = 100;
    const double TITLE_BEGIN = -25;
    const double TITLE_END = 25;
    const double SUBTITLE_Y = 80;
    const double TWEENERS_DURATION = 0.5;
    const double TWEENERS_DELAY = 0.05;
}

namespace amoebax {
    class ScoreLabel: public benzaiten::Sprite {
        public:
            typedef boost::shared_ptr<ScoreLabel> Ptr;
            static const int TEXT_OFFSET_Y = 1;
            static const int COLUMNS = 7;
            static const int HEIGHT = 26;
            static const int WIDTH = 26 * COLUMNS;

            static Ptr New (double x, double y, int pos, unsigned int score,
                    bool highlight) {
                return Ptr (new ScoreLabel (x, y, pos, score, highlight));
            }

        protected:
            ScoreLabel (double x, double y, int pos, unsigned int score,
                    bool highlight)
                : Sprite (x - WIDTH / 2, y)
            {
                using boost::lexical_cast;
                GraphicList::Ptr graphics = GraphicList::New ();

                TileMap::Ptr map = TileMap::New (
                        Game::resources ().texture ("background-score.tga"),
                        COLUMNS, 1, 26, 26);
                map->setIndex (0, 0, 0);
                for (int x = 1; x < COLUMNS - 1; ++x) {
                    map->setIndex (x, 0, 1);
                }
                map->setIndex (COLUMNS - 1, 0, 2);
                graphics->add (map, 0, 0);

                benzaiten::Font::Ptr font = Font::menu ();
                Text::Ptr pos_text =
                    Text::New (font, lexical_cast<std::string> (pos));
                pos_text->centerOrigin ();
                if (highlight) {
                    pos_text->setColorMod (178, 255, 0);
                }
                graphics->add (pos_text, 10, HEIGHT / 2 + TEXT_OFFSET_Y);

                Text::Ptr score_text = Text::New (font, lexical_cast<std::string> (score));
                score_text->alignRight ();
                if (highlight) {
                    score_text->setColorMod (178, 255, 0);
                }
                graphics->add (score_text, WIDTH - 5, HEIGHT / 2 + TEXT_OFFSET_Y);

                this->setGraphic (graphics);
            }
    };
}

HighScores::HighScores (unsigned int score, Page page)
    : Context ()
    , labels ()
    , mouse_dir (0)
    , mouse_x (-1)
    , music (Game::resources ().try_music ("menu.ogg"))
    , next ()
    , page (page)
    , subtitle_normal ()
    , subtitle_training ()
    , title ()
{
    Texture::Ptr background = Texture::New (
            Game::resources ().texture ("background-menu.tga"));
    background->setScroll (0, 0);
    this->addGraphic (background, 0, 0, -1);

    Texture::Ptr title_texture (Texture::New (
                Game::resources ().texture ("title-highscores.tga")));
    title_texture->setScroll (0, 0);
    title_texture->centerOrigin ();
    this->title = this->addGraphic (title_texture,
            Game::screen ().half_width, TITLE_BEGIN);

    Text::Ptr normal_label = Text::New (Font::menu (), "normal");
    normal_label->centerOrigin ();
    this->subtitle_normal  = this->addGraphic (normal_label,
            -Game::screen ().half_width, SUBTITLE_Y);
    Text::Ptr training_label = Text::New (Font::menu (), "training");
    training_label->centerOrigin ();
    this->subtitle_training  = this->addGraphic (training_label,
            Game::screen ().width * 2.5, SUBTITLE_Y);

    {
        Settings settings;
        this->createPage (0, settings, "normal", score, page == NORMAL);
        this->createPage (Game::screen ().width, settings, "training", score,
                page == TRAINING);
    }
}

void HighScores::begin () {
    Game::camera ().x = Game::screen ().width * this->page;

    this->addTweener (tweener::Simple::New (TITLE_BEGIN, TITLE_END,
                TWEENERS_DURATION,
                boost::bind (&Sprite::moveToY, this->title.get (), _1),
                easing::Linear::easeIn));
    this->addTweener (tweener::Simple::New (this->subtitle_normal->x (),
                Game::screen ().half_width, TWEENERS_DURATION,
                boost::bind (&Sprite::moveToX, this->subtitle_normal.get (), _1),
                easing::Linear::easeIn));
    this->addTweener (tweener::Simple::New (this->subtitle_training->x (),
                Game::screen ().width * 1.5, TWEENERS_DURATION,
                boost::bind (&Sprite::moveToX, this->subtitle_training.get (), _1),
                easing::Linear::easeIn));
    double last_y = 0.0;
    unsigned int label_index = 0;
    BOOST_FOREACH (Sprite::Ptr &label, this->labels) {
        if (label->y () < last_y) {
            label_index = 0;
        }
        last_y = label->y ();
        this->addTweener (tweener::Simple::New (
                    label->y (), label->y () - Game::screen ().height,
                    TWEENERS_DURATION,
                    boost::bind (&Sprite::moveToY, label.get (), _1),
                    easing::Linear::easeOut))->
            setDelay (label_index * TWEENERS_DELAY);
        ++label_index;
    }

    if (!Music::isPlaying () || Music::isPaused ()) {
        this->music->play ();
    }
}

void HighScores::createPage (int offset_x, Settings &settings,
        const std::string &page_name, unsigned int new_score, bool update) {
    bool highlight = false;
    for (int index = 0, y = LABELS_Y + Game::screen ().height ; index < 4;
            ++index, y += ScoreLabel::HEIGHT * 1.25) {
        unsigned int score = settings.getScore (page_name, index);
        if (update && new_score > score) {
            score = new_score;
            settings.setScore (page_name, index, score);
            update = false;
            highlight = true;
        }
        this->labels.push_back (this->add (ScoreLabel::New (
                        Game::screen ().half_width + offset_x, y, index + 1,
                        score, highlight)));
        highlight = false;
    }
}

void HighScores::update (double elapsed) {
    Context::update (elapsed);
    if (!this->next &&
            (Game::input ().pressed (benzaiten::Action::MENU_CANCEL))) {
        this->next = MainMenu::New ();

        tweener::Group::Ptr tweeners = tweener::Group::New ();
        tweeners->add (tweener::Simple::New (SDL_ALPHA_OPAQUE,
                    SDL_ALPHA_TRANSPARENT,
                    TWEENERS_DURATION / 2,
                    boost::bind (&Sprite::setAlpha, this->title.get(), _1),
                    easing::Linear::easeIn));
        tweeners->add (tweener::Simple::New (SDL_ALPHA_OPAQUE,
                    SDL_ALPHA_TRANSPARENT,
                    TWEENERS_DURATION / 2,
                    boost::bind (&Sprite::setAlpha, this->subtitle_normal.get(), _1),
                    easing::Linear::easeIn));
        tweeners->add (tweener::Simple::New (SDL_ALPHA_OPAQUE,
                    SDL_ALPHA_TRANSPARENT,
                    TWEENERS_DURATION / 2,
                    boost::bind (&Sprite::setAlpha, this->subtitle_training.get(), _1),
                    easing::Linear::easeIn));
        BOOST_FOREACH (Sprite::Ptr &label, this->labels) {
            tweeners->add (tweener::Simple::New (SDL_ALPHA_OPAQUE,
                        SDL_ALPHA_TRANSPARENT,
                        TWEENERS_DURATION / 2,
                        boost::bind (&Sprite::setAlpha, label.get(), _1),
                        easing::Linear::easeIn));
        }
        tweeners->setFinishCallback (
                boost::bind (&Game::switchTo, this->next));
        this->addTweener (tweeners);
    }

    if (Game::input ().mouse_pressed) {
        this->mouse_x = Game::input ().mouse_x;
    } else if (this->mouse_x >= 0 && Game::input ().mouse_down) {
        int offset = this->mouse_x - Game::input ().mouse_x;
        if (offset != 0) {
            this->mouse_dir = offset;
        }
        Game::camera ().x = clamp (Game::camera ().x + offset, 0.0,
                double(Game::screen ().width));
        this->mouse_x = Game::input ().mouse_x;
    } else if (this->mouse_x >= 0 && Game::input ().mouse_released) {
        if (this->mouse_dir != 0) {
            this->page = this->mouse_dir > 0 ? TRAINING : NORMAL;
            this->addTweener (tweener::Simple::New (Game::camera ().x,
                        Game::screen ().width * this->page,
                        TWEENERS_DURATION / 2, easing::Linear::easeIn));
            this->mouse_dir = 0;
        }
    }
}
