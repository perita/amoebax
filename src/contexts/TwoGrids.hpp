//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_CONTEXTS_TWO_GRIDS_HPP)
#define GEISHA_STUDIOS_AMOEBAX_CONTEXTS_TWO_GRIDS_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include <benzaiten/Context.hpp>
#include <benzaiten/Music.hpp>
#include <boost/shared_ptr.hpp>
#include "../interfaces/GridObserver.hpp"
#include "../interfaces/TwoGridsObserver.hpp"
#include "../sprites/Player.hpp"

namespace amoebax {
    class TwoGrids: public benzaiten::Context, GridObserver {
        public:
            typedef boost::shared_ptr<TwoGrids> Ptr;

            static Ptr New (TwoGridsObserver *observer,
                    const std::string &theme, const Player::Ptr &left_player,
                    unsigned int left_score, const Player::Ptr &right_player,
                    unsigned int right_score) {
                return Ptr (new TwoGrids (observer, theme, left_player,
                            left_score, right_player, right_score));
            }

            virtual void begin ();
            unsigned int getLeftScore () const;
            unsigned int getRightScore () const;
            virtual void end ();
            virtual void onActivePair (Grid &grid);
            virtual void onFull (Grid &grid);
            virtual void onGhosts (Grid &grid, double x, double y,
                    int to_remove, int to_add);
            virtual void update (double elapsed);

        protected:
            TwoGrids (TwoGridsObserver *observer, const std::string &theme,
                    const Player::Ptr &left_player, unsigned int left_score,
                    const Player::Ptr &rigth_player, unsigned int right_score);

        private:
            void startGame ();

            benzaiten::Music::Ptr music;
            TwoGridsObserver *observer;
            Player::Ptr player_left;
            Player::Ptr player_right;
            bool playing;
            benzaiten::Context::Ptr prev;
            bool show_ready_go;
    };
}

#endif // !GEISHA_STUDIOS_AMOEBAX_CONTEXTS_TWO_GRIDS_HPP
