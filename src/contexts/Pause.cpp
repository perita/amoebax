//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Pause.hpp"
#include <cassert>
#include <benzaiten/Game.hpp>
#include <benzaiten/tweeners/Group.hpp>
#include <benzaiten/tweeners/Simple.hpp>
#include <benzaiten/tweeners/easing/Back.hpp>
#include <benzaiten/tweeners/easing/Linear.hpp>
#include <boost/assign.hpp>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include "../Font.hpp"
#include "../sprites/MenuOption.hpp"

using namespace amoebax;
using namespace benzaiten;

namespace {
    const double MENU_FIRST_ROW = 100;
    const double MENU_ROW_HEIGHT = 40;
    const double TWEENERS_DURATION = 0.25;
    const double TWEENERS_DELAY = 0.05;

    struct Option {
        enum Id {
            Continue,
            End,
        };
        const Id id;
        const std::string label;

        Option (Id id, const std::string &label)
            : id (id)
            , label (label)
        {
        }
    };
}

Pause::Pause (unsigned int score, HighScores::Page scores_page)
    : Context ()
    , alpha (0)
    , next ()
    , prev (Game::activeContext ())
    , score (score)
    , scores_page (scores_page)
{
}

void Pause::begin () {
    this->addTweener (tweener::Simple::New (this->alpha, 0.0,
                SDL_ALPHA_OPAQUE / 2.0,
                TWEENERS_DURATION, easing::Linear::easeIn));

    using boost::assign::list_of;
    const std::list<Option> options =
        list_of (Option (Option::Continue, "continue game"))
                (Option (Option::End, "end game"));
    benzaiten::Font::Ptr font (Font::menu ());
    unsigned int option_index = 0;
    BOOST_FOREACH (const Option &option, options) {
        Sprite::Ptr sprite = this->add (MenuOption::New (*this,
                        option.id, Game::screen ().half_width,
                        Game::screen ().height + 10, font, option.label));
        this->options.push_back (sprite);
        this->addTweener (tweener::Simple::New (sprite->y (),
                    MENU_FIRST_ROW + MENU_ROW_HEIGHT * option_index,
                    TWEENERS_DURATION,
                    boost::bind (&Sprite::moveToY, sprite.get (), _1),
                    easing::Back::easeOut))->
            setDelay (option_index * TWEENERS_DELAY);
        ++option_index;
    }
}

void Pause::onOption (int option) {
    // Nothing to do if we already have a next context.
    if (this->next) {
        return;
    }

    double alpha_target = this->alpha;
    switch (option) {
        case Option::Continue:
            this->next = this->prev;
            alpha_target = SDL_ALPHA_TRANSPARENT;
            break;

        case Option::End:
            this->next = HighScores::New (this->score, this->scores_page);
            alpha_target = SDL_ALPHA_OPAQUE;
            break;

        default:
            assert (false && option && "No valid menu option");
            break;
    }

    tweener::Group::Ptr tweeners = tweener::Group::New ();
    tweeners->setFinishCallback (boost::bind (&Pause::switchToNext, this));
    tweeners->add (tweener::Simple::New (this->alpha, alpha_target,
                TWEENERS_DURATION, easing::Linear::easeIn));
    BOOST_FOREACH (Sprite::Ptr &option, this->options) {
        tweeners->add (tweener::Simple::New (SDL_ALPHA_OPAQUE,
                    SDL_ALPHA_TRANSPARENT, TWEENERS_DURATION,
                    boost::bind (&Sprite::setAlpha, option.get (), _1),
                    easing::Linear::easeIn));
    }
    this->addTweener (tweeners);
}

void Pause::render (SDL_Renderer &renderer, const SDL_Rect *clip) const {
    if (prev) {
        prev->render (renderer, clip);
    }

    SDL_SetRenderDrawColor (&renderer, 0, 0, 0, this->alpha);
    SDL_BlendMode mode;
    SDL_GetRenderDrawBlendMode (&renderer, &mode);
    SDL_SetRenderDrawBlendMode (&renderer, SDL_BLENDMODE_BLEND);
    SDL_RenderFillRect (&renderer, clip);
    SDL_SetRenderDrawBlendMode (&renderer, mode);

    Context::render (renderer, clip);
}

void Pause::switchToNext () {
    Game::switchTo (this->next);
}

void Pause::update (double elapsed) {
    Context::update (elapsed);
    if (Game::input ().pressed (benzaiten::Action::MENU_CANCEL)) {
        this->onOption (Option::Continue);
    }
}
