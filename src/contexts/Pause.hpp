//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_CONTEXTS_PAUSE_HPP)
#define GEISHA_STUDIOS_AMOEBAX_CONTEXTS_PAUSE_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <vector>
#include <benzaiten/Context.hpp>
#include <benzaiten/Sprite.hpp>
#include <boost/shared_ptr.hpp>
#include "HighScores.hpp"
#include "../interfaces/OptionObserver.hpp"

namespace amoebax {
    class Pause: public benzaiten::Context, OptionObserver {
        public:
            typedef boost::shared_ptr<Pause> Ptr;

            static Ptr New (unsigned int score, HighScores::Page scores_page) {
                return Ptr (new Pause (score, scores_page));
            }
            virtual void begin ();
            virtual void onOption (int option);
            virtual void render (SDL_Renderer &renderer,
                    const SDL_Rect *clip) const;
            virtual void update (double elapsed);

        protected:
            Pause (unsigned int score, HighScores::Page scores_page);

        private:
            void switchToNext ();

            double alpha;
            benzaiten::Context::Ptr next;
            std::vector<benzaiten::Sprite::Ptr> options;
            benzaiten::Context::Ptr prev;
            unsigned int score;
            HighScores::Page scores_page;
    };
}

#endif // !GEISHA_STUDIOS_AMOEBAX_CONTEXTS_PAUSE_HPP
