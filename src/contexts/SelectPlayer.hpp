//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_CONTEXT_SELECT_PLAYER_HPP)
#define GEISHA_STUDIOS_AMOEBAX_CONTEXT_SELECT_PLAYER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <benzaiten/Context.hpp>
#include <benzaiten/Music.hpp>
#include <benzaiten/Sprite.hpp>
#include <benzaiten/Tweener.hpp>
#include <boost/shared_ptr.hpp>

namespace amoebax {
    class SelectPlayer: public benzaiten::Context {
        public:
            typedef boost::shared_ptr<SelectPlayer> Ptr;

            static Ptr New () { return Ptr (new SelectPlayer ()); }

            virtual void begin ();
            virtual void update (double elapsed);

        protected:
            SelectPlayer ();

        private:
            benzaiten::Tweener::Ptr removeTitle ();
            void returnToMainMenu ();
            void selectKim ();
            void selectTom ();

            benzaiten::Sprite::Ptr kim;
            benzaiten::Music::Ptr music;
            bool selected;
            benzaiten::Sprite::Ptr title;
            benzaiten::Sprite::Ptr tom;
    };
}

#endif // !GEISHA_STUDIOS_AMOEBAX_CONTEXT_SELECT_PLAYER_HPP
