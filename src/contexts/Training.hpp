//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_CONTEXTS_TRAINING_HPP)
#define GEISHA_STUDIOS_AMOEBAX_CONTEXTS_TRAINING_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <vector>
#include <benzaiten/Context.hpp>
#include <benzaiten/Music.hpp>
#include <benzaiten/graphics/Text.hpp>
#include <boost/shared_ptr.hpp>
#include "../interfaces/GridObserver.hpp"
#include "../sprites/Player.hpp"

namespace amoebax {
    class Training: public benzaiten::Context, GridObserver {
        public:
            typedef boost::shared_ptr<Training> Ptr;

            static Ptr New () { return Ptr (new Training); }
            virtual void begin ();
            virtual void end ();
            virtual void onActivePair (Grid &grid);
            virtual void onFull (Grid &grid);
            virtual void onGhosts (Grid &grid, double x, double y,
                    int to_remove, int to_add);
            virtual void update (double elapsed);

        protected:
            Training ();

        private:
            struct Level
            {
                /// The minimum score points to have to be in this level.
                unsigned int score;
                /// The probability to add ghost amoebas each time (in %).
                unsigned int probability;
                /// The max number of ghost amobeas that can be added each time.
                unsigned int max_ghosts;

                Level (unsigned int score, unsigned int probability,
                        unsigned int ghosts)
                    : score (score)
                    , probability (probability)
                    , max_ghosts (ghosts)
                {
                }
            };

            void startGame ();

            size_t current_level;
            benzaiten::Text::Ptr level_label;
            std::vector<Level> levels;
            benzaiten::Music::Ptr music;
            Player::Ptr player;
            bool playing;
    };
}

#endif // !GEISHA_STUDIOS_AMOEBAX_CONTEXTS_TRAINING_HPP
