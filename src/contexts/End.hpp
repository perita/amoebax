//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_CONTEXT_END_HPP)
#define GEISHA_STUDIOS_AMOEBAX_CONTEXT_END_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include <benzaiten/Context.hpp>
#include <benzaiten/Music.hpp>
#include <benzaiten/Sprite.hpp>
#include <boost/shared_ptr.hpp>

namespace amoebax {
    class End: public benzaiten::Context {
        public:
            typedef boost::shared_ptr<End> Ptr;

            static Ptr New (const std::string &player, unsigned int score) {
                return Ptr (new End (player, score));
            }

            virtual void begin ();
            virtual void update (double elapsed);

        protected:
            End (const std::string &player, unsigned int score);

        private:
            benzaiten::Music::Ptr music;
            benzaiten::Sprite::Ptr player;
            benzaiten::Sprite::Ptr podium;
            int player_height;
            unsigned int score;
            benzaiten::Sprite::Ptr title;
            benzaiten::Sprite::Ptr versus;
    };
}

#endif // !GEISHA_STUDIOS_AMOEBAX_CONTEXT_END_HPP
