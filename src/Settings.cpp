//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Settings.hpp"
#include <algorithm>
#include <limits>
#include <boost/lexical_cast.hpp>

using namespace amoebax;
using namespace benzaiten;

namespace {
    const int NUM_SCORES = 4;
}

Settings::Settings ()
    : SettingsBase ("amoebax")
{
}

unsigned int Settings::getScore (const std::string &type, int index) const {
    const std::string name = boost::lexical_cast<std::string> (index);
    int score = std::max (0, this->getInteger (type, name, (NUM_SCORES - index) * 1000));
    return static_cast<unsigned int> (score);
}

void Settings::setScore (const std::string &type, int index,
        unsigned int new_score) {
    // Move all the others
    if (index < (NUM_SCORES - 1)) {
        this->setScore (type, index + 1, this->getScore (type, index));
    }
    const std::string name = boost::lexical_cast<std::string> (index);
    new_score = std::min (new_score,
            static_cast<unsigned int>(std::numeric_limits<int>::max ()));
    int score = static_cast<int>(new_score);
    this->setInteger (type, name, score);
}
