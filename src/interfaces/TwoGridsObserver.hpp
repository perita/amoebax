//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_INTERFACES_TWO_GRIDS_OBSERVER_HPP)
#define GEISHA_STUDIOS_AMOEBAX_INTERFACES_TWO_GRIDS_OBSERVER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

namespace amoebax {
    class TwoGrids;

    class TwoGridsObserver {
        public:
            enum Side {
                LEFT,
                RIGHT
            };

            virtual ~TwoGridsObserver () { }

            virtual void onFull (TwoGrids &grids, Side side) = 0;
            virtual void onPause (TwoGrids &grids) = 0;
    };
}

#endif // !GEISHA_STUDIOS_AMOEBAX_INTERFACES_TWO_GRIDS_OBSERVER_HPP
