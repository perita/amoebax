//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Grid.hpp"
#include <cmath>
#include <benzaiten/Context.hpp>
#include <benzaiten/Game.hpp>
#include <benzaiten/Utils.hpp>
#include <boost/foreach.hpp>
#include "sprites/ChainLabel.hpp"
#include "Font.hpp"

using namespace amoebax;
using namespace benzaiten;

namespace {
    const int FIRST_COLUMN = 0;
    const int FIRST_ROW = 3;
    const int CELL_HEIGHT = Amoeba::HEIGHT;
    const int CELL_WIDTH = Amoeba::WIDTH;
    const int CELLS_COLUMNS = 6;
    const int CELLS_ROWS = 12 + FIRST_ROW;
    const int CELLS_X =  0;
    const int CELLS_Y = 20 - FIRST_ROW * CELL_HEIGHT;
    const int CENTER_COLUMN = CELLS_COLUMNS / 2;
    const int LAST_COLUMN = CELLS_COLUMNS - 1;
    const int LAST_ROW = CELLS_ROWS - 1;
    const double QUEUE_LEFT_SPEED = CELL_WIDTH * 4;
    const double QUEUE_UP_SPEED = CELL_HEIGHT * 4;
    const int QUEUE_RIGHT_X = 102;
    const int QUEUE_LEFT_X = -21;
    const int QUEUE_Y = 21;
    const int PAIR_START = FIRST_ROW - 2;
    const int GHOST_X = 0;
    const int GHOST_Y = 0;

    const int FACE_NONE = 0x00;
    const int FACE_UP = 0x01;
    const int FACE_RIGHT = 0x02;
    const int FACE_BOTTOM = 0x04;
    const int FACE_LEFT = 0x08;
    const int FACE_ALL = FACE_UP | FACE_RIGHT | FACE_BOTTOM | FACE_LEFT;

    const double REMOVE_TIME = 0.5;
    const double BLINK_TIME = 0.05;

    const double SPEED_UP_SCORE = 16;

    // Half a second to fall to the bottom.
    const int FALL_SPEED = Amoeba::HEIGHT * CELLS_ROWS * 2;
}

Grid::Grid (GridObserver &observer, double x, double y, double score_x,
        double score_y, bool queue_on_right, unsigned int initial_score)
    : active_pair ()
    , blink_time (0.0)
    , cells (benzaiten::TileMap::New (
                Game::resources ().texture ("amoebas.tga"),
                CELLS_COLUMNS, CELLS_ROWS, CELL_WIDTH, CELL_HEIGHT))
    , context (0)
    , chain (0)
    , floating ()
    , font (Font::score ())
    , ghost_row (benzaiten::TileMap::New (
                Game::resources ().texture ("amoebas.tga"),
                CELLS_COLUMNS, CELLS_ROWS, CELL_WIDTH, CELL_HEIGHT))
    , ghosts_recently_added (false)
    , groups_to_remove ()
    , observers ()
    , queue (3)
    , queue_on_right (queue_on_right)
    , queue_x (queue_on_right ? QUEUE_RIGHT_X : QUEUE_LEFT_X)
    , remove_time (0.0)
    , score (Score::New (initial_score, score_x, score_y, font))
    , virtualy_removed_ghosts (0)
    , waiting_ghosts (0)
    , x (x)
    , y (y)
{
    this->addObserver (observer);

    double indent = this->queue_on_right ? CELL_WIDTH / 2 : CELL_WIDTH / -2;
    for (size_t pair = 0 ; pair < this->queue.capacity () ; ++pair) {
        this->queue.push_back (Pair::New (
                    x + this->queue_x + (pair > 0 ? indent : 0),
                    y + QUEUE_Y + (pair * 2 + 1) * CELL_HEIGHT,
                    0, 0));
    }
}

void Grid::addGhosts (int ghosts, bool wait_next_turn) {
    this->waiting_ghosts = std::min (30, this->waiting_ghosts + ghosts);
    this->refreshGhostRow ();
    this->ghosts_recently_added = wait_next_turn;
}

void Grid::addGroupToRemove (const Group &group) {
    if (group.cells.size () > 3) {
        this->groups_to_remove.push_back (group);
        this->remove_time = REMOVE_TIME;
    }
}

void Grid::addObserver (GridObserver &observer) {
    this->observers.insert (&observer);
}

bool Grid::addToExistingGroupToRemove (int column, int row, Amoeba::Color color) {
    if (color == Amoeba::WHITE) {
        return false;
    }
    BOOST_FOREACH (Group &group, this->groups_to_remove) {
        if (this->getAmoebaColor (group.cells.begin ()->x, group.cells.begin ()->y) == color) {
            BOOST_FOREACH (const Cell &cell, group.cells) {
                if (    (cell.x == column - 1 && cell.y == row - 0) ||
                        (cell.x == column - 0 && cell.y == row - 1) ||
                        (cell.x == column + 1 && cell.y == row + 0) ||
                        (cell.x == column + 0 && cell.y == row + 1)) {
                    group.cells.insert (Cell (column, row));
                    return true;
                }
            }
        }
    }
    return false;
}

void Grid::adjustAmoebaFace (int column, int row, Amoeba::Color color,
        bool recurse) {

    // For white (ghost) amoebas and these that aren't of the passed color, no
    // face is available.
    if (color == Amoeba::WHITE || this->getAmoebaColor (column, row) != color) {
        return;
    }

    int face = FACE_NONE;
    if (this->getAmoebaColor (column, row - 1) == color) {
        face |= FACE_UP;
        if (recurse) {
            this->adjustAmoebaFace (column, row - 1, color, !recurse);
        }
    }
    if (this->getAmoebaColor (column + 1, row) == color) {
        face |= FACE_RIGHT;
        if (recurse) {
            this->adjustAmoebaFace (column + 1, row, color, !recurse);
        }
    }
    if (this->getAmoebaColor (column, row + 1) == color) {
        face |= FACE_BOTTOM;
        if (recurse) {
            this->adjustAmoebaFace (column, row + 1, color, !recurse);
        }
    }
    if (this->getAmoebaColor (column - 1, row) == color) {
        face |= FACE_LEFT;
        if (recurse) {
            this->adjustAmoebaFace (column - 1, row, color, !recurse);
        }
    }
    this->cells->setIndex (column, row, (color << 4) | face);
}

bool Grid::canGhostsFall () const {
    return this->waiting_ghosts > 0 && this->chain == 0 &&
        !this->ghosts_recently_added;
}

bool Grid::canMoveLeft () const {
    return this->active_pair && this->masterColumn () > 0 &&
        this->moonColumn () > 0 &&
        this->isEmpty (this->masterColumn () - 1, this->masterRow ()) &&
        this->isEmpty (this->moonColumn () - 1, this->moonRow ());
}

bool Grid::canMoveRight () const {
    return this->active_pair && this->masterColumn () < CELLS_COLUMNS - 1 &&
        this->moonColumn () < CELLS_COLUMNS - 1 &&
        this->isEmpty (this->masterColumn () + 1, this->masterRow ()) &&
        this->isEmpty (this->moonColumn () + 1, this->moonRow ());
}

void Grid::checkGhostsToRemoveAndSend () {
    unsigned int chain_score = 0;
    BOOST_FOREACH (Group &group, this->groups_to_remove) {
        if (!group.checked) {
            chain_score += this->getGroupScore (group);
            group.checked = true;
        }
    }
    if (chain_score > 0) {
        int chain_ghosts = std::ceil (chain_score / 70.0);
        int to_remove = std::min (chain_ghosts,
                this->waiting_ghosts - this->virtualy_removed_ghosts);
        int to_add = chain_ghosts - to_remove;

        if (to_remove > 0 || to_add > 0) {
            Cell cell = *(this->groups_to_remove.begin ()->cells.begin ());
            double x = this->x + CELLS_X + cell.x * CELL_WIDTH + CELL_WIDTH / 2;
            double y = this->y + CELLS_Y + cell.y * CELL_HEIGHT + CELL_HEIGHT / 2;
            BOOST_FOREACH (GridObserver *observer, this->observers) {
                observer->onGhosts (*this, x, y, to_remove, to_add);
            }
        }
        this->virtualy_removed_ghosts += to_remove;
    }
}

void Grid::enqueueActivePair () {
    this->active_pair->reset ();
    double indent = this->queue_on_right ? CELL_WIDTH / 2 : CELL_WIDTH / -2;
    this->active_pair->moveTo (this->x + this->queue_x + indent,
            this->y + QUEUE_Y + ((this->queue.capacity () - 1) * 2 + 1) *
                CELL_HEIGHT);
    this->active_pair->setToRowAndColumn (0, 0);
    this->queue.push_back (this->active_pair);
    this->active_pair.reset ();
}

std::vector<Pair *> Grid::getAllPairs () const {
    std::vector<Pair *> pairs;
    if (this->active_pair) {
        pairs.push_back (this->active_pair.get ());
    }
    BOOST_FOREACH (const Pair::Ptr &pair, this->queue) {
        pairs.push_back (pair.get ());
    }
    return pairs;
}

Amoeba::Color Grid::getAmoebaColor (int column, int row) const {
    int color = this->cells->getIndex (column, row);
    if (color < 0) {
        return static_cast<Amoeba::Color> (-1);
    }
    return static_cast<Amoeba::Color> (color >> 4);
}

unsigned int Grid::getGroupScore (const Group &group) const {
    return ((2 * group.cells.size () - 4) * 10) << this->chain;
}

Grid::State Grid::getState () const {
    Grid::State state (boost::extents[CELLS_COLUMNS][CELLS_ROWS]);
    for (int y = 0 ; y < CELLS_ROWS ; ++y) {
        for (int x = 0 ; x < CELLS_COLUMNS ; ++x) {
            int cell = this->cells->getIndex (x, y);
            state[x][y] = (cell > -1) ? (cell >> 4) : -1;
        }
    }
    return state;
}

void Grid::makeAmoebaFall (int column, int row) {
    if (row < 0) {
        return;
    }
    int color_value = this->getAmoebaColor (column, row);
    if (color_value < 0) {
        return;
    }
    Amoeba::Color color = static_cast<Amoeba::Color> (color_value);

    int x = this->x + CELLS_X + column * CELL_WIDTH;
    int y = this->y + CELLS_Y + row * CELL_HEIGHT;
    this->floating.push_back (Amoeba::New (x, y, color));
    this->removeAmoeba (column, row);
    this->adjustAmoebaFace (column - 1, row, color, false);
    this->adjustAmoebaFace (column + 1, row, color, false);
    this->makeAmoebaFall (column, row - 1);
}

void Grid::makeGhostsFall () {
    int y = this->y + CELLS_Y + FIRST_ROW * CELL_HEIGHT;
    std::vector<int> columns;
    columns.reserve (CELLS_COLUMNS);

    while (this->waiting_ghosts > 0) {
        if (columns.empty ()) {
            int x = this->x + CELLS_X;
            for (int col = FIRST_COLUMN; col <= LAST_COLUMN;
                    ++col, x += CELL_WIDTH) {
                columns.push_back (x);
            }
            std::random_shuffle (columns.begin (), columns.end ());
            y -= CELL_HEIGHT;
        }
        this->floating.push_back (
                Amoeba::New (columns.back (), y, Amoeba::WHITE));
        columns.pop_back ();
        --this->waiting_ghosts;
    }
    this->refreshGhostRow ();
}

void Grid::makeGroup (int column, int row, Amoeba::Color color,
        Group &group) {
    // If the color is white (ghost), no groups can be formed.
    if (color == Amoeba::WHITE) {
        return;
    }

    // Adjust the face of the first amoeba.
    if (group.cells.empty ()) {
        this->adjustAmoebaFace (column, row, color);
    }

    Cell cell (column, row);
    // Don't pass again for the same cell.
    if (column < 0 || column > LAST_COLUMN|| row < 0 || row > LAST_ROW ||
            group.cells.find (cell) != group.cells.end ()) {
        return;
    }
    if (this->getAmoebaColor (column, row) == color) {
        group.cells.insert (cell);
        this->makeGroup (column - 1, row, color, group);
        this->makeGroup (column, row - 1, color, group);
        this->makeGroup (column + 1, row, color, group);
        this->makeGroup (column, row + 1, color, group);
    }
}

void Grid::moveLeft () {
    if (this->canMoveLeft ()) {
        this->active_pair->moveLeft ();
    }
}

void Grid::moveRight () {
    if (this->canMoveRight ()) {
        this->active_pair->moveRight ();
    }
}

void Grid::refreshGhostRow () {
    int remaining = this->waiting_ghosts;
    for (int col = 0; col < CELLS_COLUMNS; ++col) {
        int weight = 0;
        if (remaining > 0) {
            if (remaining <= CELLS_COLUMNS - col) {
                weight = 1;
            } else {
                weight = std::ceil (remaining / (double)(CELLS_COLUMNS - col));
            }
            remaining -= weight;
        }
        this->ghost_row->setIndex (col, 0,
                (Amoeba::WHITE << 4) + (FACE_ALL - weight));
    }
}

void Grid::removeAmoeba (int column, int row) {
    this->cells->setIndex (column, row, -1);
    this->cells->setVisible (column, row, true);
}

void Grid::removeGhosts (int ghosts) {
    int to_remove = std::min (this->waiting_ghosts, ghosts);
    this->virtualy_removed_ghosts -= to_remove;
    this->waiting_ghosts -= to_remove;
    this->refreshGhostRow ();
}

void Grid::removeGroup (const Group &group) {
    int max_x = std::numeric_limits<int>::min ();
    int max_y = std::numeric_limits<int>::min ();
    int min_x = std::numeric_limits<int>::max ();
    int min_y = std::numeric_limits<int>::max ();
    BOOST_FOREACH (const Cell &cell, group.cells) {
        // Remove white (ghost) amoebas adjacent to this one.
        if (this->getAmoebaColor (cell.x - 1, cell.y) == Amoeba::WHITE) {
            this->removeAmoeba (cell.x - 1, cell.y);
            this->makeAmoebaFall (cell.x - 1, cell.y - 1);
        }
        if (this->getAmoebaColor (cell.x, cell.y - 1) == Amoeba::WHITE) {
            this->removeAmoeba (cell.x, cell.y - 1);
            this->makeAmoebaFall (cell.x, cell.y - 2);
        }
        if (this->getAmoebaColor (cell.x + 1, cell.y) == Amoeba::WHITE) {
            this->removeAmoeba (cell.x + 1, cell.y);
            this->makeAmoebaFall (cell.x + 1, cell.y - 1);
        }
        if (this->getAmoebaColor (cell.x, cell.y + 1) == Amoeba::WHITE) {
            this->removeAmoeba (cell.x, cell.y + 1);
            // No need to make fall any amoeba; this is under the one
            // removed!
        }

        // Make the amoeba on top fall only if it is not of the same color.
        if (this->getAmoebaColor (cell.x, cell.y) !=
                this->getAmoebaColor (cell.x, cell.y - 1)) {
            this->makeAmoebaFall (cell.x, cell.y - 1);
        }
        this->removeAmoeba (cell.x, cell.y);
        max_x = std::max (max_x, cell.x);
        max_y = std::max (max_y, cell.y);
        min_x = std::min (min_x, cell.x);
        min_y = std::min (min_y, cell.y);
    }
    unsigned int chainScore = this->getGroupScore (group);
    this->score->increase (chainScore);
    this->context->add (ChainLabel::New (
                this->x + CELLS_X + (min_x + (max_x - min_x) / 2) * CELL_WIDTH +
                    CELL_WIDTH / 2,
                this->y + CELLS_Y + (min_y + (max_y - min_y) / 2) * CELL_HEIGHT +
                    CELL_HEIGHT / 2,
                this->chain + 1, this->font));
}

void Grid::removeObserver (GridObserver &observer) {
    this->observers.erase (&observer);
}

void Grid::render (SDL_Renderer &renderer, const SDL_Rect *clip) const {
    SDL_Rect grid_clip = makeRect (this->x + CELLS_X,
            this->y + CELLS_Y + FIRST_ROW * CELL_HEIGHT,
            CELLS_COLUMNS * CELL_WIDTH, CELLS_ROWS * CELL_HEIGHT);
    this->cells->render (renderer, &grid_clip, this->x + CELLS_X,
            this->y + CELLS_Y);
    if (this->active_pair) {
        this->active_pair->render (renderer, &grid_clip);
    }
    BOOST_FOREACH (const Amoeba::Ptr &amoeba, this->floating) {
        amoeba->render (renderer, &grid_clip);
    }

    SDL_Rect queue_clip = makeRect (this->x + this->queue_x,
            this->y + QUEUE_Y, CELL_WIDTH, CELL_HEIGHT * 4);
    BOOST_FOREACH (const Pair::Ptr &pair, this->queue) {
        pair->render (renderer, &queue_clip);
    }

    this->ghost_row->render (renderer, clip, this->x + GHOST_X,
            this->y + GHOST_Y);
    this->score->render (renderer, clip);
}

void Grid::rotate () {
    if (!this->active_pair) {
        return;
    }

    if (this->moonRow () > this->masterRow () && !this->canMoveLeft ()) {
        if (this->canMoveRight ()) {
            this->active_pair->moveRight ();
            this->active_pair->rotate ();
        }
    } else if (this->moonRow () < this->masterRow () &&
            !this->canMoveRight ()) {
        if (this->canMoveLeft ()) {
            this->active_pair->moveLeft ();
            this->active_pair->rotate ();
        }
    } else {
        this->active_pair->rotate ();
    }
}

void Grid::setAmoebaColor (int column, int row, Amoeba::Color color) {
    this->cells->setIndex (column, row, color << 4);
}

void Grid::toggleGroup (const Group &group) {
    BOOST_FOREACH (const Cell &cell, group.cells) {
        this->cells->setVisible (cell.x, cell.y,
                !this->cells->isVisible (cell.x, cell.y));
    }
}

void Grid::update (double elapsed) {
    if (this->floating.empty ()) {
        if (this->groups_to_remove.empty ()) {
            if (this->active_pair) {
                this->updateActivePair (elapsed);
            } else {
                if (this->canGhostsFall ()) {
                    this->makeGhostsFall ();
                } else {
                    // Check whether the row before the first on the central
                    // column is occuped.  If so, the game is over.
                    if (this->isOccuped (CENTER_COLUMN, FIRST_ROW - 1)) {
                        BOOST_FOREACH (GridObserver *observer, this->observers) {
                            observer->onFull (*this);
                        }
                    } else {
                        this->updateQueue (elapsed);
                    }
                }
            }
        } else {
            this->updateGroupsToRemove (elapsed);
        }
    } else {
        this->updateFloatingAmoebas (elapsed);
    }
    this->checkGhostsToRemoveAndSend ();
}

void Grid::updateActivePair (double elapsed) {
    if (this->active_pair->isSpeedUp ()) {
        this->score->increase (SPEED_UP_SCORE * elapsed);
    }
    this->active_pair->update (elapsed);
    bool masterCollides = this->isOccuped (this->masterColumn (),
            this->masterRow () + 1);
    bool moonCollides = this->isOccuped (this->moonColumn (),
            this->moonRow () + 1);
    if (this->active_pair->getRow () == LAST_ROW ||
            masterCollides || moonCollides) {
        this->setAmoebaColor (this->masterColumn (), this->masterRow (),
                this->masterColor ());
        this->setAmoebaColor (this->moonColumn (), this->moonRow (),
                this->moonColor ());
        // Check again to see if either the master or the moon
        // are floating.
        bool masterFloats = this->masterRow () != LAST_ROW &&
            this->isEmpty (this->masterColumn (), this->masterRow () + 1);
        bool moonFloats = this->moonRow () != LAST_ROW &&
            this->isEmpty (this->moonColumn (), this->moonRow () + 1);
        // First remove all possible floating amoebas.
        if (masterFloats) {
            this->makeAmoebaFall (this->masterColumn (), this->masterRow ());
        }
        if (moonFloats) {
            this->makeAmoebaFall (this->moonColumn (), this->moonRow ());
        }
        // Now make groups of same-colored amoebas.
        Group amoebas;
        this->makeGroup (this->masterColumn (), this->masterRow (),
                this->masterColor (), amoebas);
        this->addGroupToRemove (amoebas);

        // In the case of the moon, first check if it is not already
        // included in the previous group; otherwise only update its
        // face.
        Cell moon_cell (this->moonColumn (), this->moonRow ());
        if (amoebas.cells.find (moon_cell) == amoebas.cells.end ()) {
            amoebas.cells.clear ();
            this->makeGroup (moon_cell.x, moon_cell.y,
                    this->moonColor (), amoebas);
            this->addGroupToRemove (amoebas);
        } else {
            this->adjustAmoebaFace (moon_cell.x, moon_cell.y,
                    this->moonColor ());
        }

        this->enqueueActivePair ();
    }
}

void Grid::updateFloatingAmoebas (double elapsed) {
    std::list<Amoeba::Ptr>::iterator amoeba = this->floating.begin ();
    while (amoeba != this->floating.end ()) {
        (*amoeba)->fall (FALL_SPEED * elapsed);
        int column = ((*amoeba)->x () - CELLS_X - this->x) / CELL_WIDTH;
        int row = ((*amoeba)->y () - CELLS_Y - this->y) / CELL_HEIGHT;
        // Account for slow systems where the amoeba can skip a row or two.
        while (row > LAST_ROW || this->isOccuped (column, row)) {
            --row;
        }
        if (row == LAST_ROW || this->isOccuped (column, row + 1)) {
            Amoeba::Color color = (*amoeba)->getColor ();
            // The amoebas that fall way below the first row aren't considered;
            // they must be white (ghost) amoebas.
            if (row >= 0) {
                this->setAmoebaColor (column, row, color);
                if (color != Amoeba::WHITE) {
                    if (this->addToExistingGroupToRemove (column, row, color)) {
                        this->adjustAmoebaFace (column, row, color);
                    } else {
                        Group amoebas;
                        this->makeGroup (column, row, color, amoebas);
                        this->addGroupToRemove (amoebas);
                    }
                }
            }
            amoeba = this->floating.erase (amoeba);
        } else {
            ++amoeba;
        }
    }
}

void Grid::updateGroupsToRemove (double elapsed) {
    if (this->remove_time > 0.0) {
        if (this->blink_time > 0.0) {
            this->blink_time -= elapsed;
        } else {
            this->blink_time = BLINK_TIME;
            BOOST_FOREACH (const Group &group, this->groups_to_remove) {
                this->toggleGroup(group);
            }
        }
        this->remove_time -= elapsed;
    } else {
        std::list<Group>::iterator group = this->groups_to_remove.begin ();
        while (group != this->groups_to_remove.end ()) {
            this->removeGroup (*group);
            group = this->groups_to_remove.erase (group);
        }
        ++this->chain;
    }
}

void Grid::updateQueue (double elapsed) {
    if (this->queue[0]->y () > this->y + QUEUE_Y - CELL_HEIGHT) {
        int index = 0;
        BOOST_FOREACH (Pair::Ptr &pair, this->queue) {
            double speed_x = 0.0;
            if (index == 1 && this->queue_on_right &&
                    pair->x () > (this->x + this->queue_x)) {
                speed_x = -QUEUE_LEFT_SPEED;
            } else if (index == 1 && !this->queue_on_right &&
                    pair->x () < (this->x + this->queue_x)) {
                speed_x = QUEUE_LEFT_SPEED;
            }
            pair->advance (speed_x * elapsed, -QUEUE_UP_SPEED * elapsed);
            if (index == 1 && ((this->queue_on_right &&
                        pair->x () < (this->x + this->queue_x)) || (
                        !this->queue_on_right &&
                        pair->x () > (this->x + this->queue_x)))) {
                pair->moveTo (this->x + this->queue_x, pair->y ());
            }
            pair->setToRowAndColumn (0, 0);
            ++index;
        }
    } else {
        this->active_pair = this->queue[0];
        this->active_pair->moveTo (this->x + CELLS_X, this->y + CELLS_Y);
        this->active_pair->setToRowAndColumn (PAIR_START, CENTER_COLUMN);
        this->queue.pop_front ();
        int y_pos = this->y + QUEUE_Y + CELL_HEIGHT;
        BOOST_FOREACH (Pair::Ptr &pair, this->queue) {
            pair->moveTo (pair->x (), y_pos);
            pair->setToRowAndColumn (0, 0);
            y_pos += CELL_HEIGHT * 2;
        }
        this->chain = 0;
        this->ghosts_recently_added = false;
        BOOST_FOREACH (GridObserver *observer, this->observers) {
            observer->onActivePair (*this);
        }
    }
}
