//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Simple.hpp"

using namespace amoebax::player;

namespace {
    const double TIME_TO_MOVE = 0.5;
}

Simple::Simple (unsigned char max_deep, bool speed_up)
    : Computer (max_deep, speed_up, TIME_TO_MOVE / (speed_up ? 2 : 1))
{
}

int Simple::computeScore (const PositionResult &result) const {
    int score = 0;

    score -= 1 * result.max_height;
    score -= 5 * result.middle_column_height;
    score += 10 * result.num_remaining_groups;
    score += 20 * result.chains[0];
    if (result.middle_column_height > 12) {
        score -= 10000;
    }

    return score;
}
