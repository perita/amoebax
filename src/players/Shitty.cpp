//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Shitty.hpp"
#include <cstdlib>

using namespace amoebax::player;
using namespace benzaiten;

namespace {
    const double TIME_TO_MOVE = 0.7; // in seconds.
}

Shitty::Shitty ()
    : Computer (0, false, TIME_TO_MOVE)
{
}

int Shitty::computeScore (const PositionResult &result) const {
    return rand ();
}
