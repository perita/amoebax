//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_PLAYERS_COMPUTER_HPP)
#define GEISHA_STUDIOS_AMOEBAX_PLAYERS_COMPUTER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include "../sprites/Player.hpp"
#include <set>
#include <vector>
#include <benzaiten/Thread.hpp>

namespace amoebax { namespace player {

    class Computer: public Player {
        public:
            virtual void onActivePair (Grid &grid);
            virtual ~Computer ();

        protected:
            struct PositionResult {
                std::vector<int> chains;
                int ghosts_removed;
                int max_height;
                int middle_column_height;
                int num_remaining_groups;

                PositionResult ()
                    : chains ()
                    , ghosts_removed (0)
                    , max_height (0)
                    , middle_column_height (0)
                    , num_remaining_groups (0)
                {
                }
            };

            Computer (unsigned char max_deep, bool speed_up,
                    double time_to_move);

            virtual int computeScore (const PositionResult &result) const = 0;
            virtual void doUpdate (Grid &grid, double elapsed);

        private:
            enum MoonRotation {
                MoonBottom,
                MoonLeft,
                MoonRight,
                MoonTop
            };

            struct Group {
                std::vector<Cell> amoebas;
                std::vector<Cell> ghosts;
            };

            struct Floating {
                Amoeba::Color color;
                Cell cell;

                Floating (int column, int row, Amoeba::Color color)
                    : color (color)
                    , cell (column, row)
                { }
            };

            struct Position {
                int master_column;
                MoonRotation moon_rotation;
                int score;
            };

            Position checkAllPositionsFor (
                    std::vector<Pair *>::const_iterator begin,
                    std::vector<Pair *>::const_iterator end,
                    const Grid::State &grid_state);
            int checkPairMovement (const Pair &pair) const;
            static PositionResult checkPosition (const Pair &pair,
                    const Position &position, Grid::State &state);
            void findBestPosition (const std::vector<Pair *> &pairs,
                    const Grid::State &grid_state);
            static void makeGroup (Amoeba::Color group_color, int width, int height,
                    int x, int y, const Grid::State &state,
                    std::set<Cell, CellCompare> &visited, Group &group);
            bool movePair (const Pair &pair, Grid &grid) const;
            bool mustRotatePair (const Pair &pair) const;
            static std::vector<Group> pushFloatingDown (
                    const std::vector<Floating> &floating, Grid::State &state);
            static void removeGroup (const Group &group, Grid::State &state,
                    std::vector<Floating> &floating);
            static int updateAI (void *data);

            benzaiten::SDL::Cond ai_cond;
            benzaiten::SDL::Mutex ai_mutex;
            SDL_Thread *ai_thread;
            bool ai_thread_end;
            Position best_position;
            bool found_best_position;
            Grid *grid;
            bool is_waiting_active_pair;
            unsigned char max_deep;
            bool speed_up;
            double time_to_move;
            double time_to_next_move;
    };
} }

#endif // !GEISHA_STUDIOS_AMOEBAX_PLAYERS_COMPUTER_HPP
