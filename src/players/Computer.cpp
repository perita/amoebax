//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Computer.hpp"
#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <limits>
#include <vector>
#include <SDL_timer.h>
#include <boost/assign.hpp>
#include <boost/foreach.hpp>

using namespace amoebax::player;
using namespace benzaiten;

namespace {
    static const int COLS_DIM = 0;
    static const int ROWS_DIM = 1;
}

Computer::Computer (unsigned char max_deep, bool speed_up, double time_to_move)
    : Player ()
    , ai_cond (SDL::cond ())
    , ai_mutex (SDL::mutex ())
    , ai_thread (0)
    , ai_thread_end (false)
    , best_position ()
    , found_best_position (false)
    , grid (0)
    , is_waiting_active_pair (true)
    , max_deep (max_deep)
    , speed_up (speed_up)
    , time_to_move (time_to_move)
    , time_to_next_move (time_to_move)
{
    assert (this->max_deep < 3 && "AI position search too deep");
#if ! defined(EMSCRIPTEN)
    this->ai_thread = SDL_CreateThread (Computer::updateAI, "AI", this);
    assert (this->ai_thread != 0);
#endif
}

Computer::~Computer () {
#if ! defined(EMSCRIPTEN)
    {
        SDL::ScopedLock lock (this->ai_mutex);
        this->ai_thread_end = true;
        SDL::signal (this->ai_cond);
    }
    int return_value;
    SDL_WaitThread (this->ai_thread, &return_value);
#endif
}

Computer::Position Computer::checkAllPositionsFor (
        std::vector<Pair *>::const_iterator begin,
        std::vector<Pair *>::const_iterator end,
        const Grid::State &grid_state) {

    assert (begin != end && "Invalid iterators to check the positions for");

    // Initialize the best position with the current pair's position
    // but with the worst position score.
    Position best;
    PositionResult best_result;
    best.master_column = (*begin)->getMasterColumn ();
    best.moon_rotation = MoonTop;
    best.score = std::numeric_limits<int32_t>::min ();
    std::vector<Pair *>::const_iterator next = begin + 1;

    std::vector<MoonRotation> rotations =
        boost::assign::list_of<MoonRotation>
            (MoonTop)(MoonLeft)(MoonRight)(MoonBottom);
    BOOST_FOREACH (MoonRotation rotation, rotations) {
        Position position;
        position.moon_rotation = rotation;
        int columns = grid_state.shape ()[COLS_DIM];
        int master_column_end = columns - (rotation == MoonRight ? 1 : 0);
        for (position.master_column = rotation == MoonLeft ? 1 : 0 ;
                position.master_column < master_column_end ;
                ++position.master_column) {
            Grid::State state = grid_state;
            PositionResult result =
                Computer::checkPosition (*(*begin), position, state);
            position.score = this->computeScore (result);
            if (next != end) {
                Position subbest = this->checkAllPositionsFor (next, end, state);
                position.score += subbest.score;
            }
            if (position.score > best.score ||
                    (position.score == best.score && rand () % 2 == 0)) {
                best = position;
                best_result = result;
            }
        }
    }

    return best;
}

int Computer::checkPairMovement (const Pair &pair) const {
    return this->best_position.master_column - pair.getMasterColumn ();
}

Computer::PositionResult Computer::checkPosition (const Pair &pair,
        const Position &position, Grid::State &state) {
    const int cols = state.shape ()[COLS_DIM];
    const int rows = state.shape ()[ROWS_DIM];

    std::vector<Floating> floating;
    // Only in the case that the Moon is at the bottom I should put the
    // moon first to the list of floating amoebas.
    if (position.moon_rotation == MoonBottom) {
        floating.push_back (Floating (position.master_column, 0,
                    pair.getMoonColor ()));
        floating.push_back (Floating (position.master_column, 0,
                    pair.getMasterColor ()));
    } else {
        floating.push_back (Floating (position.master_column, 0,
                    pair.getMasterColor ()));
        int relative = (position.moon_rotation == MoonTop ? 0 : 1) *
                       (position.moon_rotation == MoonLeft ? -1 : 1);
        floating.push_back (Floating (position.master_column + relative, 0,
                    pair.getMoonColor ()));
    }

    // Repeat until there is no floating amoeba left.
    PositionResult result;
    do {
        std::vector<Group> groups =
            Computer::pushFloatingDown (floating, state);
        floating.clear ();

        int chain_size = 0;
        BOOST_FOREACH (const Group &group, groups) {
            chain_size += group.amoebas.size ();
            result.ghosts_removed += group.ghosts.size ();
            Computer::removeGroup (group, state, floating);
        }
        result.chains.push_back (chain_size);
    } while (!floating.empty ());

    // Check height
    for (int col = 0 ; col < cols ; ++col) {
        int height = 0;
        for (int row = rows - 1 ; row >= 0 && state[col][row] > -1 ; --row) {
            ++height;
        }
        result.max_height = std::max (result.max_height, height);
        if (col == cols / 2) {
            result.middle_column_height = height;
        }
    }

    // Remaining groups (all of them are < 4)
    std::set<Cell, CellCompare> visited;
    for (int col = 0 ; col < cols ; ++col) {
        for (int row = 0 ; row < rows ; ++row) {
            int color = state[col][row];
            if (color > -1) {
                Group group;
                Computer::makeGroup (static_cast<Amoeba::Color> (color),
                        cols, rows, col, row, state, visited, group);
                if (group.amoebas.size () > 1) {
                    ++result.num_remaining_groups;
                }
            }
        }
    }

    return result;
}

void Computer::doUpdate (Grid &grid, double elapsed) {
#if defined(EMSCRIPTEN)
    Computer::updateAI(this);
#endif // EMSCRIPTEN
    // Can't do nothing until the next active pair.
    if (this->is_waiting_active_pair) {
        return;
    }

    Pair::Ptr active_pair = grid.getActivePair ();
    if (!active_pair) {
        return;
    }
    if (this->found_best_position) {
        if (this->time_to_next_move > 0) {
            this->time_to_next_move -= elapsed;
        } else {
            this->time_to_next_move += this->time_to_move;
            if (!this->movePair (*active_pair, grid)) {
                this->is_waiting_active_pair = true;
                grid.speedUp (this->speed_up);
            }
        }
    }
}

void Computer::findBestPosition (const std::vector<Pair *> &pairs,
        const Grid::State &grid_state) {
    this->best_position = checkAllPositionsFor (pairs.begin (),
            pairs.begin () + this->max_deep + 1, grid_state);
    this->found_best_position = true;
}

void Computer::makeGroup (Amoeba::Color group_color, int width, int height,
        int x, int y, const Grid::State &state,
        std::set<Cell, CellCompare> &visited, Group &group) {
    if (x < 0 || y < 0 || x >= width || y >= height || state[x][y] < 0) {
        return;
    }

    Cell cell (x, y);
    if (visited.find (cell) != visited.end ()) {
        return;
    }
    Amoeba::Color color = static_cast<Amoeba::Color> (state[x][y]);
    if (color == Amoeba::WHITE) {
        visited.insert (cell);
        group.ghosts.push_back (cell);
    } else if (color == group_color) {
        visited.insert (cell);
        group.amoebas.push_back (cell);
        Computer::makeGroup (group_color, width, height, x - 1, y - 0, state,
                visited, group);
        Computer::makeGroup (group_color, width, height, x - 0, y - 1, state,
                visited, group);
        Computer::makeGroup (group_color, width, height, x + 1, y + 0, state,
                visited, group);
        Computer::makeGroup (group_color, width, height, x + 0, y + 1, state,
                visited, group);
    }
}

bool Computer::movePair (const Pair &pair, Grid &grid) const {
    if (this->mustRotatePair (pair)) {
        grid.rotate ();
    } else {
        int movement = this->checkPairMovement (pair);
        if (movement < 0) {
            int old_col = pair.getMasterColumn ();
            grid.moveLeft ();
            int new_col = pair.getMasterColumn ();
            // If couldn't move the pair, then probably it took to much time
            // to move or something and bumped against a “tower” of amoebas;
            // there is no point on keeping moving if so.
            return old_col != new_col;
        } else if (movement > 0) {
            int old_col = pair.getMasterColumn ();
            grid.moveRight ();
            int new_col = pair.getMasterColumn ();
            return old_col != new_col; // ditto.
        } else {
            return false;
        }
    }
    return true;
}

bool Computer::mustRotatePair (const Pair &pair) const {
    MoonRotation rotation = this->best_position.moon_rotation;
    return (rotation == MoonBottom &&
            pair.getMasterRow () >= pair.getMoonRow ()) ||
           (rotation == MoonLeft &&
            pair.getMasterColumn () <= pair.getMoonColumn ()) ||
           (rotation == MoonRight &&
            pair.getMasterColumn () >= pair.getMoonColumn ());
}

void Computer::onActivePair (Grid &grid) {
#if !defined(EMSCRIPTEN)
    SDL::ScopedLock (this->ai_mutex);
#endif // !EMSCRIPTEN
    this->grid = &grid;
    grid.speedUp (false);
    this->found_best_position = false;
    this->is_waiting_active_pair = false;
#if !defined(EMSCRIPTEN)
    SDL::signal (this->ai_cond);
#endif // !EMSCRIPTEN
}

std::vector<Computer::Group> Computer::pushFloatingDown (
        const std::vector<Floating> &floating, Grid::State &state) {

    const int rows = state.shape()[ROWS_DIM];
    const int cols = state.shape()[COLS_DIM];

    std::vector<Cell> cellsToCheck;
    BOOST_FOREACH (const Floating &amoeba, floating) {
        int col = amoeba.cell.x;
        int row = amoeba.cell.y;
        for ( ; row < rows && state[col][row] < 0 ; ++row ) {
            // Nothing to do here.
        }
        // If the row remains 0, then I am screwed and can't do much :'(
        if (row > 0) {
            state[col][row - 1] = amoeba.color;
            cellsToCheck.push_back (Cell (col, row - 1));
        }
    }

    std::set<Cell, CellCompare> visited;
    std::vector<Computer::Group> groups;
    BOOST_FOREACH (const Cell &cell, cellsToCheck) {
        Group group;
        Amoeba::Color color =
            static_cast<Amoeba::Color> (state[cell.x][cell.y]);
        Computer::makeGroup (color, cols, rows, cell.x, cell.y, state, visited,
                group);
        if (group.amoebas.size () >= 4) {
            groups.push_back (group);
        }
    }

    return groups;
}

void Computer::removeGroup (const Group &group, Grid::State &state,
        std::vector<Floating> &floating) {
    BOOST_FOREACH (const Cell &cell, group.amoebas) {
        if (cell.y > 0 && state[cell.x][cell.y - 1] != state[cell.x][cell.y]) {
            int y = cell.y - 1;
            while (y > 0 && state[cell.x][y] > -1) {
                Amoeba::Color color =
                    static_cast<Amoeba::Color> (state[cell.x][y]);
                floating.push_back (Floating (cell.x, y, color));
                state[cell.x][y] = -1;
                --y;
            }
        }
        state[cell.x][cell.y] = -1;
    }

    BOOST_FOREACH (const Cell &cell, group.ghosts) {
        if (cell.y > 0) {
            int y = cell.y - 1;
            while (y > 0 && state[cell.x][y] > -1) {
                Amoeba::Color color =
                    static_cast<Amoeba::Color> (state[cell.x][y]);
                floating.push_back (Floating (cell.x, y, color));
                state[cell.x][y] = -1;
                --y;
            }
        }
        state[cell.x][cell.y] = -1;
    }
}

int Computer::updateAI (void *data) {
    Computer *self = reinterpret_cast<Computer *> (data);

#if ! defined(EMSCRIPTEN)
    while (true) {
        {
            SDL::ScopedLock lock (self->ai_mutex);
            SDL::wait (self->ai_cond, self->ai_mutex);
        }
#endif // ! EMSCRIPTEN
        if (!self->is_waiting_active_pair && !self->found_best_position) {
            Pair::Ptr active_pair = self->grid->getActivePair ();
            if (active_pair) {
                self->findBestPosition (self->grid->getAllPairs (),
                        self->grid->getState ());
            }
        }
#if defined(EMSCRIPTEN)
	return 0;
#else
        if (self->ai_thread_end) {
            return 0;
        }
    }
#endif // !EMSCRIPTEN
}
