//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_PLAYERS_HUMAN_HPP)
#define GEISHA_STUDIOS_AMOEBAX_PLAYERS_HUMAN_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <boost/shared_ptr.hpp>
#include "../sprites/Player.hpp"

namespace amoebax { namespace player {
    class Human: public Player {
        public:
            typedef boost::shared_ptr<Human> Ptr;

            static Ptr New () { return Ptr (new Human ()); }

        protected:
            Human ();

            virtual void doUpdate (Grid &grid, double elapsed);

        private:
            benzaiten::Point2D<int> mouse;
            bool mouse_moved;
            double mouse_time;
    };
} }

#endif // !GEISHA_STUDIOS_AMOEBAX_PLAYERS_HUMAN_HPP
