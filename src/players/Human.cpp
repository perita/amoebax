//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Human.hpp"
#include <benzaiten/Game.hpp>

using namespace amoebax::player;
using namespace benzaiten;

namespace {
    const int HORIZONTAL_THRESHOLD = 18; // pixels
    const double ROTATE_THRESHOLD = 0.2; // seconds
    const int VERTICAL_THRESHOLD = 3; // pixels
}

Human::Human ()
    : Player ()
    , mouse (0, 0)
    , mouse_moved (false)
    , mouse_time (0.0)
{
}

void Human::doUpdate (Grid &grid, double elapsed) {
    // Regular controls (including virtual joystick)
    if (Game::input ().pressed (::Action::LEFT)) {
        grid.moveLeft ();
    } else if (Game::input ().pressed (::Action::RIGHT)) {
        grid.moveRight ();
    } else if (Game::input ().pressed (::Action::ROTATE)) {
        grid.rotate ();
    }
    bool speedUp = Game::input ().isDown (::Action::DOWN);

    // Using the mouse/finger to control the falling amoebas.
    if (Game::input ().mouse_pressed) {
        this->mouse.x = Game::input ().mouse_x;
        this->mouse.y = Game::input ().mouse_y;
        this->mouse_moved = false;
        this->mouse_time = 0.0;
    } else if (Game::input ().mouse_down) {
        this->mouse_time += elapsed;
        // Check whether to move left or right.
        int diff_x = Game::input ().mouse_x - this->mouse.x;
        if (diff_x < -HORIZONTAL_THRESHOLD) {
            grid.moveLeft ();
            this->mouse.x -= HORIZONTAL_THRESHOLD;
            this->mouse_moved = true;
        } else if (diff_x > HORIZONTAL_THRESHOLD) {
            grid.moveRight ();
            this->mouse.x += HORIZONTAL_THRESHOLD;
            this->mouse_moved = true;
        }
        // In vertical, down is the only option
        int diff_y = Game::input ().mouse_y - this->mouse.y;
        if (diff_y > VERTICAL_THRESHOLD) {
            speedUp |= true;
            this->mouse.y += VERTICAL_THRESHOLD;
            this->mouse_moved = true;
        } else {
            speedUp |= false;
        }
    } else if (Game::input ().mouse_released) {
        // Only rotate if there was no other movement and the mouse wasn't
        // clicked too much time, to avoid .
        if (!this->mouse_moved && this->mouse_time < ROTATE_THRESHOLD) {
            grid.rotate ();
        }
    }
    grid.speedUp (speedUp);
}
