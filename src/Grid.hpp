//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_GRID_HPP)
#define GEISHA_STUDIOS_AMOEBAX_GRID_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <functional>
#include <list>
#include <set>
#include <benzaiten/Font.hpp>
#include <benzaiten/Point2D.hpp>
#include <benzaiten/graphics/TileMap.hpp>
#include <boost/circular_buffer.hpp>
#include <boost/multi_array.hpp>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include "interfaces/GridObserver.hpp"
#include "sprites/Pair.hpp"
#include "sprites/Score.hpp"

namespace benzaiten {
    class Context;
}

namespace amoebax {
    typedef benzaiten::Point2D<int> Cell;

    struct CellCompare {
        bool operator() (const Cell & first, const Cell &second) const {
            int squared_first = first.x * first.x + first.y * first.y;
            int squared_second = second.x * second.x + second.y * second.y;
            if (squared_first < squared_second) {
                return true;
            }
            if (squared_second < squared_first) {
                return false;
            }
            if (first.x < second.x) {
                return true;
            }
            if (second.x < first.x) {
                return false;
            }
            return first.y < second.y;
        }
    };

    class Grid: public boost::noncopyable {
        public:
            typedef boost::shared_ptr<Grid> Ptr;
            typedef boost::multi_array<int, 2> State;

            static Ptr New (GridObserver &observer, double x, double y,
                    double score_x, double score_y, bool queue_on_right,
                    unsigned int initial_score = 0) {
                return Ptr (new Grid (observer, x, y, score_x, score_y,
                            queue_on_right, initial_score));
            }

            void addGhosts (int ghosts, bool wait_next_turn = true);
            void addObserver (GridObserver &observer);
            bool canGhostsFall () const;
            Pair::Ptr getActivePair () const { return this->active_pair; }
            std::vector<Pair *> getAllPairs () const;
            unsigned int getScore () const { return this->score->getValue (); }
            State getState () const;
            void moveLeft ();
            void moveRight ();
            void removeGhosts (int ghosts);
            void removeObserver (GridObserver &observer);
            void render (SDL_Renderer &renderer, const SDL_Rect *clip) const;
            void rotate ();
            void speedUp (bool enable) {
                if (this->active_pair) {
                    this->active_pair->setSpeedUp (enable);
                }
            }
            void setContext (benzaiten::Context *context) {
                this->context = context;
            }
            void update (double elapsed);

        protected:
            Grid (GridObserver &observer, double x, double y,
                    double score_x, double score_y, bool queue_on_right,
                    unsigned int initial_score = 0);

        private:
            struct Group {
                std::set<Cell, CellCompare> cells;
                bool checked;

                Group ()
                    : cells ()
                    , checked (false)
                 {
                }
            };

            bool addToExistingGroupToRemove (int column, int row,
                    Amoeba::Color color);
            void addGroupToRemove (const Group &amoebas);
            void adjustAmoebaFace (int column, int row, Amoeba::Color color,
                    bool recurse = true);
            bool canMoveLeft () const;
            bool canMoveRight () const;
            void checkGhostsToRemoveAndSend ();
            void enqueueActivePair ();
            Amoeba::Color getAmoebaColor (int column, int row) const;
            unsigned int getGroupScore (const Group &group) const;

            bool isEmpty (int column, int row) const {
                return !this->isOccuped (column, row);
            }

            bool isOccuped (int column, int row) const {
                return this->cells->getIndex (column, row) > -1;
            }

            void makeAmoebaFall (int column, int row);
            void makeGhostsFall ();
            void makeGroup (int column, int row, Amoeba::Color color,
                    Group &group);

            Amoeba::Color masterColor () const {
                return this->active_pair->getMasterColor ();
            }

            int masterColumn () const {
                return this->active_pair->getMasterColumn ();
            }

            int masterRow () const {
                return this->active_pair->getMasterRow ();
            }

            Amoeba::Color moonColor () const {
                return this->active_pair->getMoonColor ();
            }

            int moonColumn () const {
                return this->active_pair->getMoonColumn ();
            }

            int moonRow () const {
                return this->active_pair->getMoonRow ();
            }

            void refreshGhostRow ();
            void removeAmoeba (int row, int column);
            void removeGroup (const Group &group);
            void setAmoebaColor (int row, int column, Amoeba::Color color);
            void toggleGroup (const Group &group);
            void updateActivePair (double elapsed);
            void updateFloatingAmoebas (double elapsed);
            void updateGroupsToRemove (double elapsed);
            void updateQueue (double elapsed);

            Pair::Ptr active_pair;
            double blink_time;
            benzaiten::TileMap::Ptr cells;
            benzaiten::Context *context;
            int chain;
            std::list<Amoeba::Ptr> floating;
            benzaiten::Font::Ptr font;
            benzaiten::TileMap::Ptr ghost_row;
            bool ghosts_recently_added;
            std::list<Group> groups_to_remove;
            std::set<GridObserver *> observers;
            boost::circular_buffer<Pair::Ptr> queue;
            bool queue_on_right;
            double queue_x;
            double remove_time;
            Score::Ptr score;
            int virtualy_removed_ghosts;
            int waiting_ghosts;
            double x;
            double y;
    };
}

#endif // !GEISHA_STUDIOS_AMOEBAX_GRID_HPP
