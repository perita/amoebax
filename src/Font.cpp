//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Font.hpp"
#include <benzaiten/Game.hpp>
#include <benzaiten/fonts/BitmapFont.hpp>

using namespace amoebax;
using namespace benzaiten;

benzaiten::Font::Ptr amoebax::Font::menu () {
    static BitmapFont::Ptr font =
        BitmapFont::New (" !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~",
                Game::resources ().surface ("font-menu.tga"));
    return font;
}

benzaiten::Font::Ptr amoebax::Font::score () {
    static BitmapFont::Ptr font = BitmapFont::New (" 0123456789chain",
            Game::resources ().surface ("font-score.tga"));
    return font;
}
