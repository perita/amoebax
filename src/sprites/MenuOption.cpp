//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "MenuOption.hpp"
#include <benzaiten/Game.hpp>
#include <benzaiten/graphics/Text.hpp>

using namespace amoebax;
using namespace benzaiten;

namespace {
    const double PADDING = 20;
}

MenuOption::MenuOption (OptionObserver &observer, int id, double x, double y,
        const Font::Ptr &font, const std::string &label)
    : Sprite (x, y)
    , id (id)
    , observer (observer)
{
    Text::Ptr text (Text::New (font, label));
    text->centerOrigin ();
    this->setGraphic (text);
    unsigned int width = font->width (label);
    this->setHitbox (PADDING + width / 2, PADDING, width + PADDING * 2,
            font->height () + PADDING);
}

void MenuOption::update (double elapsed) {
    if (Game::input ().mouse_pressed && this->collides (
                Point2D<double> (Game::input ().mouse_x,
                    Game::input ().mouse_y))) {
        this->observer.onOption (this->id);
    }
}
