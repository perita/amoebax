//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Pair.hpp"
#include <algorithm>
#include <benzaiten/Game.hpp>
#include <boost/assign.hpp>

using namespace amoebax;
using namespace benzaiten;

namespace {
    const int SILHOUETTE_WIDTH = 20;
    const int SILHOUETTE_HEIGHT = 20;
    const int SILHOUETTE_OFFSET_X = Amoeba::WIDTH / 2 - SILHOUETTE_WIDTH / 2;
    const int SILHOUETTE_OFFSET_Y = Amoeba::HEIGHT / 2 - SILHOUETTE_HEIGHT / 2;
    const double FALL_SPEED = Amoeba::HEIGHT * 2;
    const double MOVE_SPEED = Amoeba::HEIGHT * 10;
    const double ROTATION_SPEED = 360 * 2;
}

Pair::Pair (int x, int y, int row, int column)
    : Sprite (x, y)
    , master (Amoeba::New (x + column * Amoeba::WIDTH, y + row * Amoeba::HEIGHT))
    , master_row (row)
    , master_column (column)
    , moon (Amoeba::New (x + column * Amoeba::WIDTH, y + (row - 1) * Amoeba::HEIGHT))
    , moon_pos (UP)
    , move_target (master->x ())
    , silhouette (TextureAtlas::New (
                Game::resources ().texture ("amoebas-silhouette.tga"),
                SILHOUETTE_WIDTH, SILHOUETTE_HEIGHT))
    , speed_up (false)
{
    using boost::assign::list_of;

    const double rate = 8.0;
    this->silhouette->add ("red",
            list_of (0)(1)(2)(3)(4)(3)(2)(1)(0), rate, true);
    this->silhouette->add ("blue",
            list_of (5)(6)(7)(8)(9)(8)(7)(6)(5), rate, true);
    this->silhouette->add ("green",
            list_of (10)(11)(12)(13)(14)(13)(12)(11)(10), rate, true);
    this->silhouette->add ("orange",
            list_of (15)(16)(17)(18)(19)(18)(17)(16)(15), rate, true);
    this->silhouette->add ("purple",
            list_of (20)(21)(22)(23)(24)(23)(22)(21)(20), rate, true);
    this->reset ();
}

int Pair::getRow () const {
    return std::max (this->getMasterRow (), this->getMoonRow ());
}

int Pair::getMoonColumn () const {
    switch (this->moon_pos) {
        case LEFT:
            return this->getMasterColumn () - 1;
            break;

        case RIGHT:
            return this->getMasterColumn () + 1;
            break;

        default:
            // Fallback to return the master's column on DOWN or UP.
            break;
    }
    return this->getMasterColumn ();
}

int Pair::getMoonRow () const {
    switch (this->moon_pos) {
        case UP:
            return this->getMasterRow () - 1;
            break;

        case DOWN:
            return this->getMasterRow () + 1;
            break;

        default:
            // Fallback to return the master's row on LEFT or RIGHT.
            break;
    };
    return this->getMasterRow ();
}

void Pair::move (int column) {
    this->master_column = column;
    this->move_target = this->x () + column * Amoeba::WIDTH;
}

void Pair::moveLeft () {
    this->move (this->getMasterColumn () - 1);
}

void Pair::moveRight () {
    this->move (this->getMasterColumn () + 1);
}

void Pair::render (SDL_Renderer &renderer, const SDL_Rect *clip) const {
    this->silhouette->render (renderer, clip,
            this->master->x () + SILHOUETTE_OFFSET_X,
            this->master->y () + SILHOUETTE_OFFSET_Y);
    this->master->render (renderer, clip);
    this->moon->render (renderer, clip);
}

void Pair::reset () {
    this->speed_up = false;
    this->moon_pos = UP;
    this->master->reset ();
    this->moon->reset ();
    switch (this->master->getColor ()) {
        case Amoeba::RED:
            this->silhouette->play ("red");
            break;

        case Amoeba::BLUE:
            this->silhouette->play ("blue");
            break;

        case Amoeba::GREEN:
            this->silhouette->play ("green");
            break;

        case Amoeba::ORANGE:
            this->silhouette->play ("orange");
            break;

        case Amoeba::PURPLE:
            this->silhouette->play ("purple");
            break;

        default:
            assert ("Invalid amoeba color");
            break;
    }
}

void Pair::rotate () {
    if (!this->moon->isOrbiting ()) {
        switch (this->moon_pos) {
            case DOWN:
                this->moon_pos = LEFT;
                this->moon->setOrbit (90.0, 180.0);
                break;

            case LEFT:
                this->moon_pos = UP;
                this->moon->setOrbit (180.0, 270.0);
                break;

            case RIGHT:
                this->moon_pos = DOWN;
                this->moon->setOrbit (0.0, 90.0);
                break;

            case UP:
            default:
                this->moon_pos = RIGHT;
                this->moon->setOrbit (270.0, 360.0);
                break;
        }
    }
}

void Pair::setSpeedUp (bool enable) {
    this->speed_up =  enable;
}

void Pair::setToRowAndColumn (int row, int column) {
    this->master_row = row;
    this->master_column = column;

    this->master->moveTo (
            this->x () + column * Amoeba::WIDTH,
            this->y () + row * Amoeba::HEIGHT);
    this->moon->moveTo (
            this->x () + this->getMoonColumn () * Amoeba::WIDTH,
            this->y () + this->getMoonRow () * Amoeba::HEIGHT);
    this->move_target = this->master->x ();
}

void Pair::update (double elapsed) {
    this->silhouette->update (elapsed);
    this->master->fall (FALL_SPEED * elapsed * (this->isSpeedUp () ? 6 : 1));
    while (this->master->y () - (this->y () + this->master_row * Amoeba::HEIGHT) >=
            Amoeba::HEIGHT) {
        ++this->master_row;
    }
    if (this->master->x () != this->move_target) {
        if (this->master->x () > this->move_target) {
            this->master->advance (-MOVE_SPEED * elapsed, 0.0);
            if (this->master->x () < this->move_target) {
                this->master->moveTo (this->move_target, this->master->y ());
            }
        } else {
            this->master->advance (MOVE_SPEED * elapsed, 0.0);
            if (this->master->x () > this->move_target) {
                this->master->moveTo (this->move_target, this->master->y ());
            }
        }
    }
    this->moon->orbitAround (*(this->master), ROTATION_SPEED * elapsed);
}
