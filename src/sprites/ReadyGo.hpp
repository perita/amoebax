//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_SPRITES_READY_GO_HPP)
#define GEISHA_STUDIOS_AMOEBAX_SPRITES_READY_GO_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <benzaiten/Sound.hpp>
#include <benzaiten/Sprite.hpp>
#include <benzaiten/Timer.hpp>
#include <benzaiten/graphics/TextureAtlas.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>

namespace amoebax {
    class ReadyGo: public benzaiten::Sprite {
        public:
            typedef boost::function<void (void)> Callback;
            typedef boost::shared_ptr<ReadyGo> Ptr;

            static Ptr New (double x, double y, Callback callback = Callback ()) {
                return Ptr (new ReadyGo (x, y, callback));
            }

            virtual void added ();

        protected:
            ReadyGo (double x, double y, Callback callback = Callback ());

        protected:
            bool remove (benzaiten::Timer &timer);
            bool showGo (benzaiten::Timer &timer);

            benzaiten::TextureAtlas::Ptr atlas;
            Callback callback;
            benzaiten::Sound::Ptr sfx_ready;
            benzaiten::Sound::Ptr sfx_go;
    };
}

#endif // !GEISHA_STUDIOS_AMOEBAX_SPRITES_READY_GO_HPP
