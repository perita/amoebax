//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_SPRITES_SIGN_HPP)
#define GEISHA_STUDIOS_AMOEBAX_SPRITES_SIGN_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <benzaiten/Sprite.hpp>
#include <benzaiten/Sound.hpp>
#include <boost/shared_ptr.hpp>

namespace amoebax {
    class Sign: public benzaiten::Sprite {
        public:
            typedef boost::shared_ptr<Sign> Ptr;

            enum Type {
                WIN,
                LOSE,
                GAMEOVER
            };

            static Ptr New (Type type, double x, double y, double dest_y,
                    const benzaiten::Sound::Ptr &sound) {
                return Ptr (new Sign (type, x, y, dest_y, sound));
            }

            virtual void added ();

        protected:
            Sign (Type type, double x, double y, double dest_y,
                    const benzaiten::Sound::Ptr &sound);

        private:
            double dest_y;
            benzaiten::Sound::Ptr sound;
    };
}

#endif // !GEISHA_STUDIOS_AMOEBAX_SPRITES_SIGN_HPP
