//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Player.hpp"
#include <cassert>

using namespace amoebax;
using namespace benzaiten;

Player::Player ()
    : Sprite (0.0, 0.0)
    , grid ()
{
    this->setActive (false);
}

void Player::added () {
    this->grid->setContext (this->context);
}

Grid &Player::getGrid () const {
    assert (this->grid);
    return *(this->grid);
}

bool Player::isHerGrid (const Grid &grid) const {
    return this->grid.get () == &grid;
}

void Player::onActivePair (Grid &grid) {
    // Nothing to do.
}

void Player::onFull (Grid &grid) {
    // Nothing to do.
}

void Player::onGhosts (Grid &grid, double x, double y, int to_remove,
       int to_add) {
    // Nothing to do.
}

void Player::render (SDL_Renderer &renderer, const SDL_Rect *clip) const {
    assert (this->grid);
    this->grid->render (renderer, clip);
}

void Player::setGrid (const Grid::Ptr &grid) {
    assert (grid);
    if (this->grid) {
        this->grid->removeObserver (*this);
    }
    this->grid = grid;
    this->grid->addObserver (*this);
}

void Player::update (double elapsed) {
    assert (this->grid);
    this->doUpdate (*(this->grid), elapsed);
    this->grid->update (elapsed);
}
