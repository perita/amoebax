//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Label.hpp"
#include <benzaiten/Context.hpp>

using namespace amoebax;
using namespace benzaiten;

namespace {
    const double DISPLAY_TIME = 0.5;
    const int LABEL_DISTANCE = 16;
    const double UP_SPEED = -70.0;
}

Label::Label (int x, int y)
    : Sprite (x, y)
    , dest_y (y - LABEL_DISTANCE)
    , display_time (DISPLAY_TIME)
{
    this->setLayer (10);
}

void Label::update (double elapsed) {
    if (this->y () > this->dest_y) {
        this->advance (0.0, UP_SPEED * elapsed);
    } else if (this->display_time > 0.0) {
        this->display_time -= elapsed;
    } else {
        this->context->remove (this);
    }
}
