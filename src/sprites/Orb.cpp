//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Orb.hpp"
#include <algorithm>
#include <cassert>
#include <benzaiten/Game.hpp>
#include <boost/assign.hpp>

using namespace amoebax;
using namespace benzaiten;

namespace {
    const double CURVES_DURATION = 0.5;
    const double INITIAL_WAIT_TIME = 0.5;
    const double VERTICAL_DISPLACEMENT = 32;
}

Orb::Orb (double x, double y)
    : Sprite (x, y)
    , atlas (TextureAtlas::New (Game::resources ().texture ("orb.tga"), 32))
    , current_curve (0)
    , curves ()
    , state (INITIAL)
    , initial_wait_time (INITIAL_WAIT_TIME)
{
    using boost::assign::list_of;
    this->atlas->add ("Initial", list_of<unsigned int>(0)(1), 15, true);
    this->atlas->add ("Move", 2);
    this->atlas->add ("End", list_of<unsigned int>(3)(4)(5), 10, false);

    this->atlas->play ("Initial");
    this->atlas->centerOrigin ();
    this->setGraphic (this->atlas);
}

void Orb::addDestination (double x, double y, FinishCallback callback) {
    Orb::Curve::Point destination (x, y);
    Orb::Curve::Point source (this->x (), this->y ());
    if (this->curves.size () > 0) {
        source = this->curves[this->curves.size () - 1].getDestination ();
    }
    Orb::Curve::Point middle (source);
    if (destination.y < source.y) {
        middle.x -= VERTICAL_DISPLACEMENT * ((destination.x < source.x) ? -1 : 1);
        middle.y -= VERTICAL_DISPLACEMENT;
    } else {
        middle.x += (destination.x - source.x) / 2;
        middle.y += VERTICAL_DISPLACEMENT * 2;
    }
    this->curves.push_back (
            Curve (source, middle, destination, CURVES_DURATION, callback));
}
void Orb::update (double elapsed) {
    switch (this->state) {
        case INITIAL:
            if (this->initial_wait_time > 0.0) {
                this->initial_wait_time -= elapsed;
            } else {
                this->state = MOVE;
                this->atlas->play ("Move");
            }
            break;

        case MOVE:
            if (this->current_curve < this->curves.size ()) {
                Curve &curve = this->curves[this->current_curve];
                Curve::Point point = curve.update (elapsed);
                this->moveTo (point.x, point.y);
                if (curve.isFinished ()) {
                    ++this->current_curve;
                }
            } else {
                this->state = END;
                this->atlas->play ("End");
            }
            break;

        case END:
            if (!this->atlas->isPlaying ()) {
                this->context->remove (this);
            }
            break;

        default:
            assert (false && "Invalid orb state");
            break;
    };
}

Orb::Curve::Point Orb::Curve::update (double elapsed) {
    if (!this->isFinished ()) {
        const double time = std::min(this->duration - this->current_time, elapsed);
        this->current_time += time;
        const double dt = this->current_time / this->duration;
        const double dt_inv = 1 - dt;

        if (this->isFinished () && this->callback) {
            this->callback ();
        }

        return this->source * (dt_inv * dt_inv) + this->middle * (2 * dt_inv * dt) +
            this->destination * (dt * dt);
    }
    return this->destination;
}
