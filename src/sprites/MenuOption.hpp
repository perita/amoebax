//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_SPRITES_MENU_OPTION_HPP)
#define GEISHA_STUDIOS_AMOEBAX_SPRITES_MENU_OPTION_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include <benzaiten/Font.hpp>
#include <benzaiten/Sprite.hpp>
#include <boost/shared_ptr.hpp>
#include "../interfaces/OptionObserver.hpp"

namespace amoebax {
    class MenuOption: public benzaiten::Sprite {
        public:
            typedef boost::shared_ptr<MenuOption> Ptr;

            static Ptr New (OptionObserver &observer, int id, double x,
                    double y, const benzaiten::Font::Ptr &font,
                    const std::string &label) {
                return Ptr (new MenuOption (observer, id, x, y, font, label));
            }

            virtual void update (double elapsed);

        protected:
            MenuOption (OptionObserver &observer, int id, double x, double y,
                    const benzaiten::Font::Ptr &font,
                    const std::string &label);

        private:
            int id;
            OptionObserver &observer;
    };
}

#endif // !GEISHA_STUDIOS_AMOEBAX_SPRITES_MENU_OPTION_HPP
