//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "LevelUp.hpp"
#include <benzaiten/Game.hpp>
#include <benzaiten/graphics/Texture.hpp>

using namespace amoebax;
using namespace benzaiten;

LevelUp::LevelUp (int x, int y)
    : Label (x, y)
    , sfx (Game::resources ().try_sound ("levelup.ogg"))
{
    Texture::Ptr texture =
        Texture::New (Game::resources ().texture ("levelup.tga"));
    texture->centerOrigin ();
    this->setGraphic (texture);
}

void LevelUp::added () {
    this->sfx->play ();
}
