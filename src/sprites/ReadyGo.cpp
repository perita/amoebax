//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "ReadyGo.hpp"
#include <benzaiten/Game.hpp>
#include <boost/bind.hpp>

using namespace amoebax;
using namespace benzaiten;

namespace {
    const int READY_FRAME = 0;
    const int GO_FRAME = 1;
    const double READY_TIME = 0.5;
    const double GO_TIME = READY_TIME / 2;
}

ReadyGo::ReadyGo (double x, double y, Callback callback)
    : Sprite (x, y)
    , atlas (TextureAtlas::New (Game::resources ().texture ("readygo.tga"),
                71, 29))
    , callback (callback)
    , sfx_ready (Game::resources ().try_sound ("ready.ogg"))
    , sfx_go (Game::resources ().try_sound ("go.ogg"))
{
    this->type = Type::READY_GO;
    this->atlas->setFrame (READY_FRAME);
    this->atlas->centerOrigin ();
    this->setGraphic (this->atlas);
    this->setActive (false);
}

void ReadyGo::added () {
    this->sfx_ready->play ();
    Timer::Ptr timer = Timer::New (READY_TIME,
            boost::bind (&ReadyGo::showGo, this, _1));
    this->context->addTimer (timer);
}

bool ReadyGo::remove (Timer &timer) {
    this->context->remove (this);
    if (this->callback) {
        this->callback ();
    }
    return false;
}

bool ReadyGo::showGo (Timer &timer) {
    this->atlas->setFrame (GO_FRAME);
    this->sfx_go->play ();
    timer.setPeriod (GO_TIME);
    timer.setCallback (boost::bind (&ReadyGo::remove, this, _1));

    return true;
}
