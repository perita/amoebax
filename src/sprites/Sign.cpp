//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Sign.hpp"
#include <benzaiten/Game.hpp>
#include <benzaiten/graphics/TextureAtlas.hpp>
#include <benzaiten/tweeners/Simple.hpp>
#include <benzaiten/tweeners/easing/Back.hpp>
#include <boost/bind.hpp>

using namespace amoebax;
using namespace benzaiten;

namespace {
    const double DURATION = 0.3;
}

Sign::Sign (Type type, double x, double y, double dest_y,
        const Sound::Ptr &sound)
    : Sprite (x, y)
    , dest_y (dest_y)
    , sound (sound)
{
    TextureAtlas::Ptr atlas = TextureAtlas::New (
            Game::resources ().texture ("signs.tga"), 83, 32);
    atlas->setFrame (static_cast<int>(type));
    atlas->centerOrigin ();
    this->setGraphic (atlas);
    this->setLayer (10);
    this->setActive (false);
}

void Sign::added () {
    this->context->addTweener (
            tweener::Simple::New (this->y (), this->dest_y, DURATION,
                boost::bind (&Sprite::moveToY, this, _1),
                easing::Back::easeOut));
    if (this->sound) {
        this->sound->play ();
    }
}
