//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_SPRITES_AMOEBA_HPP)
#define GEISHA_STUDIOS_AMOEBAX_SPRITES_AMOEBA_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <benzaiten/Sprite.hpp>
#include <boost/shared_ptr.hpp>

namespace amoebax {
    class Amoeba: public benzaiten::Sprite {
        public:
            typedef boost::shared_ptr<Amoeba> Ptr;

            enum Color {
                RED,
                BLUE,
                GREEN,
                ORANGE,
                PURPLE,
                WHITE,
                NUM_COLORS
            };

            static const int WIDTH = 16;
            static const int HEIGHT = 16;

            static Ptr New (int x, int y) { return Ptr (new Amoeba (x, y)); }
            static Ptr New (int x, int y, Color color) {
                return Ptr (new Amoeba (x, y, color));
            }

            Color getColor () const { return this->color; }
            bool isOrbiting () const { return this->orbiting; }
            void fall (double speed);
            void orbitAround (const Amoeba &other, double speed);
            void reset (int color = -1);
            void setOrbit (double angle, double target);

        protected:
            Amoeba (int x, int y);
            Amoeba (int x, int y, Color color);

        private:
            void setupGraphic ();

            Color color;
            bool orbiting;
            double orbit_angle;
            double orbit_target;
    };
}

#endif // !GEISHA_STUDIOS_AMOEBAX_SPRITES_AMOEBA_HPP
