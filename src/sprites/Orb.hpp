//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_SPRITES_ORB_HPP)
#define GEISHA_STUDIOS_AMOEBAX_SPRITES_ORB_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <vector>
#include <benzaiten/Point2D.hpp>
#include <benzaiten/Sprite.hpp>
#include <benzaiten/graphics/TextureAtlas.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>

namespace amoebax {
    class Orb: public benzaiten::Sprite {
        public:
            typedef boost::shared_ptr<Orb> Ptr;
            typedef boost::function<void ()> FinishCallback;

            static Ptr New (double x, double y) {
                return Ptr (new Orb (x, y));
            }

            void addDestination (double x, double y, FinishCallback callback);
            virtual void update (double elapsed);

        protected:
            Orb (double x, double y);

        private:
            class Curve {
                public:
                    typedef benzaiten::Point2D<double> Point;

                    Curve (const Point &source, const Point &middle,
                            const Point &destination, double duration,
                            FinishCallback callback)
                        : callback (callback)
                        , current_time (0.0)
                        , destination (destination)
                        , duration (duration)
                        , middle (middle)
                        , source (source)
                    {
                    }

                    Point getDestination () const {
                        return this->destination;
                    }

                    bool isFinished () const {
                        return this->current_time >= this->duration;
                    }

                    Point update (double elapsed);

                private:
                    FinishCallback callback;
                    double current_time;
                    Point destination;
                    double duration;
                    Point middle;
                    Point source;
            };

            enum State {
                INITIAL,
                MOVE,
                END
            };

            benzaiten::TextureAtlas::Ptr atlas;
            std::vector<Curve>::size_type current_curve;
            std::vector<Curve> curves;
            State state;
            double initial_wait_time;
    };
}

#endif // !GEISHA_STUDIOS_AMOEBAX_SPRITES_ORB_HPP
