//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Score.hpp"
#include <boost/lexical_cast.hpp>

using namespace amoebax;
using namespace benzaiten;

Score::Score (unsigned int value, int x, int y, const Font::Ptr &font)
    : Sprite (x, y)
    , text (Text::New (font))
    , value (value)
{
    this->updateText ();
    this->text->alignRight ();
    this->setGraphic (this->text);
}

void Score::increase (double amount) {
    this->value += amount;
    this->updateText ();
}

void Score::updateText () {
    this->text->setText (boost::lexical_cast<std::string>(this->getValue ()));
}
