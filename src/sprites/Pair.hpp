//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_SPRITES_PAIR_HPP)
#define GEISHA_STUDIOS_AMOEBAX_SPRITES_PAIR_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <benzaiten/Sprite.hpp>
#include <benzaiten/graphics/TextureAtlas.hpp>
#include <boost/shared_ptr.hpp>
#include "Amoeba.hpp"

namespace amoebax {
    class Pair: public benzaiten::Sprite {
        public:
            typedef boost::shared_ptr<Pair> Ptr;

            static Ptr New (int x, int y, int row, int column) {
                return Ptr (new Pair (x, y, row, column));
            }

            Amoeba::Color getMasterColor () const { return this->master->getColor (); }
            int getMasterColumn () const { return this->master_column; }
            int getMasterRow () const { return this->master_row; }
            Amoeba::Color getMoonColor () const { return this->moon->getColor (); }
            int getMoonColumn () const;
            int getMoonRow () const;
            int getRow () const;
            bool isSpeedUp () const { return this->speed_up; }
            void moveLeft ();
            void moveRight ();
            virtual void render (SDL_Renderer &renderer, const SDL_Rect *clip) const;
            void reset ();
            void rotate ();
            void setSpeedUp (bool enable);
            void setToRowAndColumn (int row, int column);
            virtual void update (double elapsed);

        protected:
            Pair (int x, int y, int row, int column);

        private:
            enum MoonPos {
                DOWN,
                LEFT,
                RIGHT,
                UP
            };

            void move (int column);

            Amoeba::Ptr master;
            int master_row;
            int master_column;
            Amoeba::Ptr moon;
            MoonPos moon_pos;
            double move_target;
            benzaiten::TextureAtlas::Ptr silhouette;
            bool speed_up;
    };
}

#endif // !GEISHA_STUDIOS_AMOEBAX_SPRITES_PAIR_HPP
