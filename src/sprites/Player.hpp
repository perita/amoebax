//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_SPRITES_PLAYER_HPP)
#define GEISHA_STUDIOS_AMOEBAX_SPRITES_PLAYER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <benzaiten/Sprite.hpp>
#include <boost/shared_ptr.hpp>
#include "../Grid.hpp"
#include "../interfaces/GridObserver.hpp"

namespace amoebax {
    class Player: public benzaiten::Sprite, GridObserver {
        public:
            typedef boost::shared_ptr<Player> Ptr;

            virtual void added ();
            Grid &getGrid () const;
            bool isHerGrid (const Grid &grid) const;
            virtual void onActivePair (Grid &grid);
            virtual void onFull (Grid &grid);
            virtual void onGhosts (Grid &grid, double x, double y,
                    int to_remove, int to_add);
            virtual void render (SDL_Renderer &renderer,
                    const SDL_Rect *clip) const;
            void setGrid (const Grid::Ptr &grid);
            virtual void update (double elapsed);

        protected:
            Player ();

            virtual void doUpdate (Grid &grid, double elapsed) = 0;

        private:
            Grid::Ptr grid;
    };
}

#endif // !GEISHA_STUDIOS_AMOEBAX_SPRITES_PLAYER_HPP
