//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_SPRITES_SCORE_HPP)
#define GEISHA_STUDIOS_AMOEBAX_SPRITES_SCORE_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <benzaiten/Font.hpp>
#include <benzaiten/Sprite.hpp>
#include <benzaiten/graphics/Text.hpp>
#include <boost/shared_ptr.hpp>

namespace amoebax {
    class Score: public benzaiten::Sprite {
        public:
            typedef boost::shared_ptr<Score> Ptr;

            static Ptr New (unsigned int value, int x, int y,
                    const benzaiten::Font::Ptr &font) {
                return Ptr (new Score (value, x, y, font));
            }

            unsigned int getValue () const {
                return static_cast<unsigned int>(this->value);
            }

            void increase (double amount);

        protected:
            Score (unsigned int value, int x, int y,
                    const benzaiten::Font::Ptr &font);

        private:
            void updateText ();

            benzaiten::Text::Ptr text;
            // It is double because I want to increase it based on the
            // elapsed time when the pair is pushed down in the Grid.
            double value;
    };
}

#endif // !GEISHA_STUDIOS_AMOEBAX_SPRITES_SCORE_HPP
