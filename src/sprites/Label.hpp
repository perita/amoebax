//
// Amoebax -- Addictive match-3 action-puzzle game.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_AMOEBAX_SPRITES_LABEL_HPP)
#define GEISHA_STUDIOS_AMOEBAX_SPRITES_LABEL_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <benzaiten/Sprite.hpp>

namespace amoebax {
    class Label: public benzaiten::Sprite {
        public:
            virtual void update (double elapsed);

        protected:
            Label (int x, int y);

        private:
            int dest_y;
            double display_time;
    };
}

#endif // !GEISHA_STUDIOS_AMOEBAX_SPRITES_LABEL_HPP
